/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graficosapp;

import java.awt.Graphics;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 * @author Sala5
 */
public class Lienzo extends JPanel{
    
    ImageIcon Imagen;
    boolean Dibujar=true;
    int x=0, y=0;
    
    public void Init()
    {
        Imagen = new ImageIcon(this.getClass().getResource("Scott_Pilgrim.png"));
    }
    
    @Override
    public void paint(Graphics G)
    {
 
            G.drawImage(Imagen.getImage(),x,y,50,50,null);
            
    }
    
    public void SetXY(int x,int y)
    {
        this.x=x;
        this.y=y;
    }
    
}
