/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batallanaval;

/**
 *
 * @author ivazq
 */
public class TableroExt extends Tablero{
    TableroExt()
    {
        super();
    }
    
    TableroExt(int i)
    {
        super(i);
    }
    
    TableroExt(int i, int j)
    {
        super(i,j);
    }
    
    boolean estaVivo()
    {
        int i;
        for(i=0;i<Flota.length;i++)
        {
            Barco Aux;
            Aux=Flota[i];
            if (Aux.getVida()!=0)
                return true;
        }
        return false;
    }
    
    int getCasillaDisparo(int x,int y)
    {
        return Disparos[x][y];
    }
  
    
}
