/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batallanaval;

import java.util.Scanner;
import java.util.Random;

/**
 *
 * @author ivazq
 */
public class BatallaNaval {
       
    /**
     * @param args the command line arguments
     */
    static int TamTablero=10;
    static int NumBarcos=5;
    static Barco Flota1[];
    static Barco Flota2[];
    static TableroExt T1;
    static TableroExt T2;
    
    public static void main(String[] args) {
        // TODO code application logic here
        Ventana V;
        V = new Ventana();
        V.Init();
        int Opc=0;
        Scanner Texto;
        Texto=new Scanner(System.in);
        Preestablecido();
        do{
            System.out.println("Menu principal");
            System.out.println("1. Jugar");
            System.out.println("2. Configuración");
            System.out.println("5. Salir");
            Opc=Texto.nextInt();
            switch(Opc)
            {
                case 1:Jugar();break;
                case 2:Configuracion();break;
            }
            
        }while(Opc!=5);
       
    }
    
    public static void Configuracion()
    {
        int Opc=0, i, tam;
        Scanner texto;
        texto=new Scanner(System.in);
        do{
            System.out.println("Configuracion");
            System.out.println("1. Tamaño del Tablero");
            System.out.println("2. Numero de Barcos");
            System.out.println("5. Regresar");
            Opc=texto.nextInt();
            
            switch(Opc)
            {
                case 1:
                    System.out.println("Ingresa el tamaño del tablero");
                    TamTablero=texto.nextInt();
                    T1=new TableroExt(NumBarcos,TamTablero);
                    T2=new TableroExt(NumBarcos,TamTablero);
                    break;
                case 2:
                    System.out.println("Ingresa el número de barcos");
                    NumBarcos=texto.nextInt();
                    Flota1=new Barco[NumBarcos];
                    Flota2=new Barco[NumBarcos];
                    for(i=0;i<NumBarcos;i++)
                    {
                        System.out.println("Tamaño del Barco "+ (i+1));
                        tam=texto.nextInt();
                        Barco B1=new Barco();
                        B1.setLongitud(tam);
                        B1.setIdBarco(i+1);
                        B1.setVida(tam);
                        Flota1[i]=B1;
                        
                        Barco B2=new Barco();
                        B2.setLongitud(tam);
                        B2.setIdBarco(i+1);
                        B2.setVida(tam);
                        Flota2[i]=B2;
                    }
                    break;
            }
        }while(Opc!=5);
        
    }
    
    public static void Preestablecido()
    {
        int i;
        Flota1=new Barco[NumBarcos];
        Flota2=new Barco[NumBarcos];
        int TamBarcos[]={2,2,3,4,5};
        T1=new TableroExt(NumBarcos,TamTablero);
        T2=new TableroExt(NumBarcos,TamTablero);
        for(i=0;i<NumBarcos;i++)
        {
            Barco B1=new Barco();
            Barco B2=new Barco();
            
            B1.setIdBarco(i+1);
            B1.setLongitud(TamBarcos[i]);
            B1.setVida(TamBarcos[i]);
            Flota1[i]=B1;
            
            B2.setIdBarco(i+1);
            B2.setLongitud(TamBarcos[i]);
            B2.setVida(TamBarcos[i]);
            Flota2[i]=B2;
        }
        
    }
    
    public static void Jugar()
    {
        Scanner Teclado;
        int Opc;
        Teclado=new Scanner(System.in);
        do
        {
            System.out.println("Jugar");
            System.out.println("1. Colocar Barcos");           
            System.out.println("2. Mostrar Tablero");
            System.out.println("3. Iniciar Batalla");
            System.out.println("5. Regresar");
            Opc=Teclado.nextInt();
            switch(Opc)
            {
                case 1:Colocar();break;
                case 2:Mostrar();break;
                case 3:Iniciar();break;
            }
        }while(Opc!=5);
    }
    
    public static void Mostrar()
    {
        int i,j;
        for (j=0;j<TamTablero;j++)
        {
            for(i=0;i<TamTablero;i++)
            {
                System.out.print(T1.Casillas[i][j]+" ");
            
            } 
            System.out.println(" ");
        }        
    }
    
    public static void Mostrar(TableroExt TE)
    {
        int i,j;
        for (j=0;j<TamTablero;j++)
        {
            for(i=0;i<TamTablero;i++)
            {
                System.out.print(TE.getCasillaDisparo(i, j)+" ");
            
            } 
            System.out.println(" ");
        }  
        
    }
    
    public static void Colocar()
    {
        boolean P01,P02;
        int i,j,x,y,o;
        Mostrar();
        Scanner Teclado;
        Teclado=new Scanner(System.in);
        
        for(i=0;i<NumBarcos;i++)
        {
            P01=false;
            P02=false;
            Barco Aux=Flota1[i];          
            do{
                System.out.println("Ingresa las coordenadas de x y de tu barco "+ (i+1));
                x=Teclado.nextInt();
                y=Teclado.nextInt();
                System.out.println("Ingresa la orientación (0) Horizontal y (1) Vertical");
                o=Teclado.nextInt();               
                Aux.setPosicion(x, y);
                Aux.setOrientacion(o);
                P01=T1.isPosible(Aux, x, y, o);
                if (P01==true)
                    P02=T1.setBarco(Aux, x, y, o);
            }while( (P02==false) );
            T1.addBarco(Aux);
            Mostrar();
        }     
    }
    
    public static void Iniciar()
    {
        int x,y,o;
        int i;
        Random R;
        R=new Random(1);
        
        boolean Colocado=false;
        for(i=0;i<NumBarcos;i++)
        {
            Barco Aux;
            do{
                x=R.nextInt(TamTablero);
                y=R.nextInt(TamTablero);
                o=R.nextInt(2);
                
                Aux=Flota2[i];
                Aux.setPosicion(x, y);
                Aux.setOrientacion(NumBarcos);
                if (T2.isPosible(Aux, x, y, o))
                {
                    Colocado=T2.setBarco(Aux, x, y, o);
                }
            }while(Colocado!=true);
            T2.addBarco(Aux);         
        }
        
        do{
            boolean C1;
            Scanner Teclado;
            Teclado=new Scanner(System.in);
            
            do{
                System.out.println("Ingresa coordenas de disparo;");
                x=Teclado.nextInt();
                y=Teclado.nextInt();
                C1=T2.setDisparo(x, y);
                Mostrar(T2);
            }while(C1==true);
            
            do{
                System.out.println("Ingresa coordenas de disparo PC;");
                x=R.nextInt(TamTablero);
                y=R.nextInt(TamTablero);
                C1=T1.setDisparo(x, y);
                Mostrar(T1);
            }while(C1==true);
            
        }while ( (T1.estaVivo()==true) && (T2.estaVivo()==true) );
        
       
        
        
        
    }
}
