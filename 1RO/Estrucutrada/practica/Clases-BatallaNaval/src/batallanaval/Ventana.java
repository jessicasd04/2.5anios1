/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batallanaval;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 *
 * @author Sala5
 */
public class Ventana extends JFrame implements ActionListener{
    
    Contenedor T;
    Timer Tiempo;
    
    int x=0;
    
    public void Init()
    {
        T = new Contenedor();
        T.Init();
        Tiempo = new Timer(1000,this);
        Tiempo.start();;
        add(T);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300,400);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        x = x+10;
        T.SetXY(x, 0);
        T.repaint();
    }
    
}
