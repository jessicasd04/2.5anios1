/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batallanaval;

/**
 *
 * @author ivazq
 */
public class Barco {
    protected int IdBarco;
    protected int Longitud;
    int x;
    int y;
    int vida;
    String Nombre;
    int Orientacion;
    
    void setPosicion(int x, int y)
    {
        this.x=x;
        this.y=y;
    }

    public void setLongitud(int Longitud) {
        this.Longitud = Longitud;
    }
    
     public void setOrientacion(int Orientacion) {
        this.Orientacion = Orientacion;
    }

     public void setIdBarco(int IdBarco) {
        this.IdBarco = IdBarco;
    }
     
    public void setVida(int vida) {
        this.vida = vida;
    }
     
    public int getOrientacion() {
        return Orientacion;
    }

    public int getVida() {
        return vida;
    }
    
    public int getIdBarco() {
        return IdBarco;
    }

    public int getLongitud() {
        return Longitud;
    }
    
    int[] getPosicion()
    {
        int Pos[];
        Pos=new int[2];
        Pos[0]=x;
        Pos[1]=y;
        return Pos;
    }
}
