package batallanaval;

public class Tablero {
    Barco Flota[];
    int Disparos[][];
    int Casillas[][];
    int NumBarcos;
    
    
    Tablero(int NumBarcos)
    {
        Flota=new Barco[NumBarcos];
        Casillas=new int[10][10];
        Disparos=new int[10][10];
    }
    
    Tablero()
    {
        Flota=new Barco[5];
        Casillas=new int[10][10];
        Disparos=new int[10][10];
    }
    
    Tablero(int NumBarcos,int Tamaño)
    {
        Flota=new Barco[NumBarcos];
        Casillas=new int[Tamaño][Tamaño];
        Disparos=new int[Tamaño][Tamaño];
    }
    
    boolean setBarco(Barco B,int x, int y, int O)
    {
        int i;
        if (O==0)
        {
            for(i=x;i<B.getLongitud()+x;i++)
            {
                if (Casillas[i][y]!=0)
                    return false;
            }
            for(i=x;i<B.getLongitud()+x;i++)
                Casillas[i][y]=B.getIdBarco();
        }
        else
        {
            for(i=y;i<B.getLongitud()+y;i++)
            {
                if (Casillas[x][i]!=0)
                    return false;
            }
            for(i=y;i<B.getLongitud()+y;i++)
                Casillas[x][i]=B.getIdBarco();         
        }      
        return true;
    }
    
    void addBarco(Barco B)
    {
        if (NumBarcos<Flota.length)
        {
            Flota[NumBarcos]=B;
            NumBarcos++;
        }
    }
    
    boolean isPosible(Barco B, int x, int y, int o)
    {
        if (o==0)
        {
            if ( (x+(B.getLongitud()-1)) <Casillas.length )
                return true;
        }
        else
            if ( (y+(B.getLongitud()-1))<Casillas.length )
                return true;
        return false;
    }
    
    boolean setDisparo(int x, int y)
    {
        Barco B;
        if (Disparos[x][y]==0)
        {
            Disparos[x][y]=1;
            if (Casillas[x][y]!=0)
            {
                B=Flota[Casillas[x][y]-1];
                B.setVida(B.getVida()-1);
                Disparos[x][y]=2;
                
                return true;
            }
        }
        return false;
    }

    public int getCasillas(int x, int y) {
        return Casillas[x][y];
    } 
}
