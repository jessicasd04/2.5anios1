#include <cstdlib>
#include <iostream>
#include <string>
#include <string.h>
using namespace std;

//------------------------------------------------------------------------------
class electrodomestico
{
      private:
              int precioBase;
              string color;
              char ConsumoEnergetico;
              int peso;
      public:
              electrodomestico();
              electrodomestico(int, int);
              void setColor();
              string getColor();
              void setConsumo();
              char getConsumo();
              void setPeso();
              int getPeso();
              int comprobarConsumo(/*char*/);
              void comprobarColor(string);
              void precioFinal();
};
//------------------------------------------------------------------------------
class lavadora
{
     private:
             int carga;
     public:
             lavadora();
             lavadora(int, int);
             void setCarga();
             int getCarga();
             void precioFinal();
};
//------------------------------------------------------------------------------
class television
{
      private:
              int resolucion;
              bool sintonizador;
      public:
              television();
              television(int, bool);
              void setResolucion();
              int getResolucion();
              void setSintonizador();
              bool getSintonizador();
              void precioFinal();
};
//------------------------------------------------------------------------------
electrodomestico::electrodomestico()
{
    color="blanco";
    ConsumoEnergetico='F';
    precioBase=100;
    peso=5;
}

electrodomestico::electrodomestico(int precio, int peso)
{
    color="blanco";
    ConsumoEnergetico='F';
    precioBase=precio;
    peso=peso;
}
void electrodomestico::setColor()
{
    cout<<"\nColor del electrodomestico";
    cin>>color;
}
string electrodomestico::getColor()
{
    return color;
}
void electrodomestico::setConsumo()
{
    cout<<"\nConsumo energetico del electrodomestico";
    cin>>ConsumoEnergetico;
}
char electrodomestico::getConsumo()
{
    return ConsumoEnergetico;
}
void electrodomestico::setPeso()
{
    cout<<"\nPeso del electrodomestico";
    cin>>peso;
}
int electrodomestico::getPeso()
{
    return peso;
}
int electrodomestico::comprobarConsumo()
{
    int precioConsumo;
    switch(ConsumoEnergetico)
    {
        case 'A':
            precioConsumo=100;
            break;
        case 'B':
            precioConsumo=80;
            break;
        case 'C':
            precioConsumo=60;
            break;
        case 'D':
            precioConsumo=50;
            break;
        case 'E':
            precioConsumo=30;
            break;
        case 'F':
            precioConsumo=10;
    }
    return precioConsumo;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
int main(int argc, char *argv[])
{

    system("PAUSE");
    return EXIT_SUCCESS;
}
