#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>

char SerialRecieveByte(HANDLE hPort); //recive datos en el puerto
BOOL CloseSerialPort(HANDLE hPort); //cierra puerto

HANDLE OpenSerialPort(char *psPort,//es pecifica en que COM se trbajar�
                      DWORD dwBaudRate, // tasa de transferencia
                      BYTE dwByteSize, //especifica el tama�o de la cadena
                      BYTE bParity, // para asegurar si la cadena se envi� completa o no
                      BYTE bStopBits, // para la cadena de caracteres
                      DWORD Timeout) // espera cierto tiempo para recibir orden y si no recieb nada despliega un mensaje
{
    HANDLE hPort;
    DCB dcbPort;
    DWORD dwError;
    COMMTIMEOUTS commTimeouts;
    hPort = CreateFile(psPort, //se crea un archivo el cual contiene ciertos parametros para poder funcionar
                       GENERIC_READ | GENERIC_WRITE,
                       0,
                       NULL,
                       OPEN_EXISTING,
                       0,
                       NULL);
    if(hPort == INVALID_HANDLE_VALUE) //se crea un if para saber si se cre� el archivo
    {
         dwError = GetLastError();
         system("pause");
         return(hPort);
    }
    FillMemory(&dcbPort/*indica memoria de puerto*/, sizeof(dcbPort)/*indica el tama�o de la memoria designada al puerto*/,0/*indica si esta activo*/); //permite la administracion de la memoria del puerto, para esto es necesario la variable DCBport
    dcbPort.DCBlength = sizeof(dcbPort);
    GetCommState (hPort, &dcbPort); //recoge el puerto, el tama�o y el estado
    dcbPort.BaudRate = dwBaudRate; //atributo del objeto dcb port e indica la tasa de transferencia
    dcbPort.ByteSize = dwByteSize;
    dcbPort.Parity = bParity;
    dcbPort.StopBits = bStopBits;
    if(!SetCommState(hPort, &dcbPort)) //Verificar el estado del puerto
    {
        dwError = GetLastError();
        CloseSerialPort(hPort);
        hPort = INVALID_HANDLE_VALUE;
        return(hPort);
    }
    if(!SetCommTimeouts(hPort, &commTimeouts)) //Verificar el tiempo de espera
    {
        dwError = GetLastError();
        CloseSerialPort(hPort);
        hPort = INVALID_HANDLE_VALUE;
        return(hPort);
    }
    return hPort;
}

char SerialReceiveByte(HANDLE hPort)
{

   DWORD bytesread;
   DWORD *ptrbytesread = &bytesread;
   char read;
   char *ptrread = &read;
   ReadFile(hPort,ptrread,1,ptrbytesread,NULL);
   return (read);
}
BOOL CloseSerialPort(HANDLE hPort)
{
   BOOL bRes;
   DWORD dwError;
   bRes = CloseHandle(hPort);
   if(!bRes)
   {
     dwError = GetLastError();
   }
   return bRes;
}
int main()
{
   HANDLE hPort;
   char byte;
   hPort = OpenSerialPort("COM3",CBR_9600,8,NOPARITY,ONESTOPBIT,5000);
   if(hPort == INVALID_HANDLE_VALUE)
   {
      printf("Error al abrir el puerto\n");
      system("pause");
      return 1;
   }
   while(1)
   {
      printf("Lectura\n");
      byte = SerialReceiveByte(hPort);
      printf("dato %c\n",byte);
   }

   CloseSerialPort(hPort);
   system ("pause");
   return 0;
} 





