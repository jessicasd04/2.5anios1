#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */
void tri(){
	int A,b,h;
		printf("\n \tAREA DEL TRIANGULO\t \n");
		printf("\nDigite la base y la altura\n");
		scanf("%d %d",&b,&h);
		A=(b*h)/2;
		printf("\n El area del triangulo es %d",A);	
}
void cir(){
	float A,r;
		printf("\n \tAREA DE UN CIRCULO\t \n");
		printf("\n Digite el radio\n");
		scanf("%f",&r);
		A=3.1416*r*r;
	 	printf("El area del circulo es %f",A);		
}
void pen(){
	int A,a,l,P;
			printf("\n \tAREA DE UN PENTAGONO\t \n");
			printf("\nDigite el apotema y el lado\n");
			scanf("%d %d",&a,&l);
			P=5*l;
			A=(P*a)/2;
			printf("\n El area del pentagono es %d",A);
}
void oct(){
	int A,a,l,P;
			printf("\n \tAREA DE UN OCTAGONO\t \n");
			printf("\nDigite el apotema y el lado\n");
			scanf("%d %d",&a,&l);
			P=8*l;
			A=(P*a)/2;
			printf("\n El area del octagono es %d",A);
}
int main(int argc, char *argv[]) {
	int x;
	printf("\n\tBIENVENIDO AL CALCULO DE AREAS\t\n \n�QUE DESEA HACER?\n");
	printf("\n1.-AREA DE UN TRIANGULO \n2.-AREA DE UN CIRCULO\n3.-AREA DE UN PENTAGONO\n4.-AREA DE UN OCTAGONO\n \nOPRIMA EL NUMERO QUE DESEA  \n");
	scanf("%d",&x);
	switch (x){
		case 1:
		 	tri();
		 	break;
		case 2:
		    cir();
		    break;
		case 3:
			pen();
	    	break;
		case 4:
			oct();
			break;
	}
	return 0;
}
