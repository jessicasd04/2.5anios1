#include<stdio.h>
#include<iostream>
int main() {
int n,i,j,A[50][50],d[3],det, B[50][50];
    double c[50][50];
    printf("\n\t INVERSA DE MATRICES\n");
    
printf("\nDe el tamanio de la matriz ");
scanf("%d",&n);
    
    //LLENAR MATRIZ A
    printf("\nLlene la matriz \n");
     for( i=0;i<n;i++){
         for(j=0;j<n;j++){
          printf(" [%d][%d] = ",i,j);
          scanf("%d",&A[i][j]);
         }
        printf("\n");
   }
//Para imprimir matriz A
    printf("\n\tMatriz \n");
for( i=0;i<n;i++){
for(j=0; j<n;j++){
printf( " %d ",A[i][j]);
}
printf("\n");
}
    printf("\n");
    

// Determinantes
d[0]=(A[1][1]*A[2][2]-A[2][1]*A[0][2])*A[0][0];
d[1]=(A[1][0]*A[2][2]-A[2][0]*A[1][2])*A[0][1];
d[2]=(A[1][0]*A[2][1]-A[2][0]*A[1][1])*A[0][2];
det=( (A[1][1]*A[2][2]-A[2][1]*A[1][2])*A[0][0])-((A[1][0]*A[2][2]-A[2][0]*A[1][2])*A[0][1])+((A[1][0]*A[2][1]-A[2][0]*A[1][1])*A[0][2]);

printf("\n%d[(%d)(%d)-(%d)(%d)]= %d\n",A[0][0],A[1][1],A[2][2],A[1][2],A[2][1],d[0]);
printf("\n%d[(%d)(%d)-(%d)(%d)]= %d\n",A[0][1],A[1][0],A[2][2],A[1][2],A[2][0],d[1]);
printf("\n%d[(%d)(%d)-(%d)(%d)]= %d\n",A[0][2],A[1][0],A[2][1],A[1][1],A[2][0],d[2]);
printf("\n Det=(%d)-(%d)+(%d)= %d\n",d[0],d[1],d[2],det);

    //adjunta
    printf("\n transpuesta");
    B[0][0]=(A[1][1]*A[2][2]-A[2][1]*A[1][2]);
    B[0][1]=(-1*(A[1][0]*A[2][2]-A[2][0]*A[1][2]));
    B[0][2]=(A[1][0]*A[2][1]-A[2][0]*A[1][1]);
    B[1][0]=(-1*(A[0][1]*A[2][2]-A[2][1]*A[0][2]));
    B[1][1]=(A[0][0]*A[2][2]-A[2][0]*A[0][2]);
    B[1][2]=(-1*(A[1][1]*A[2][2]-A[2][1]*A[1][2]));
    B[2][0]=(A[0][1]*A[1][2]-A[1][1]*A[0][2]);
    B[2][1]=(-1*(A[0][0]*A[1][2]-A[1][0]*A[0][2]));
    B[2][0]=(A[0][0]*A[1][1]-A[1][0]*A[0][1]);
    
    
    //imprmir adjunta
    for(i=0;i<3;i++){
    printf("\n");
    for(j=0;j<3;j++){
        printf("%d",B[j][i]);
    }
}
    

//inversa 
    printf("\n Inversa de la matriz");
    c[0][1]=(-1*(A[1][1]*A[2][2]-A[2][0]*A[1][2]))*(1/det);
    c[0][2]=(A[1][0]*A[2][1]-A[2][0]*A[1][1])*(1/det);
    c[1][0]=(-1*(A[0][1]*A[2][2]-A[2][1]*A[0][2]))*(1/det);
    c[1][1]=A[0][0]*A[2][2]-A[2][0]*A[0][2];
    c[1][2]=(-1*(A[0][0]*A[2][1]-A[2][0]*A[0][1]))*(1/det);
    c[2][0]=(A[0][1]*A[1][2]-A[1][1]*A[0][2])*(1/det);
    c[2][1]=(-1*(A[0][0]*A[1][2]-A[1][0]*A[0][2]))*(1/det);
    c[2][2]=(A[0][0]*A[1][1]-A[1][0]*A[0][1])*(1/det);
  
//imprmir inversa
    for(i=0;i<n;i++){
    for(j=0;j<n;j++){
        printf("\n%f",c[i][j]);
    }
        printf("\n");
}
    }