#include <stdio.h>
#include <stdlib.h>
void serie(int);
int main(int argc, char *argv[]) {
	int elm;

	printf("\n\t Digite numero de elementos: ");
	scanf("%d",&elm);
	serie(elm);

	printf("\n\n");
	system("pause");
	return 0;
}
void serie(int n)
{
	static int a=0, b=1;
	a++;
	b++;
	if(a%2==0)
		printf(" -%d/%d ",a,b);
	else
		printf(" %d/%d ",a,b);
	if(a<n)
		serie(n);
}
