
#include <stdio.h>
#include <stdlib.h>

int main()
{
  int A[3][3], B[3][3], C[3][3], i, j, k;
  printf("Escribe el valor de la matriz A\n");
  for(i=0;i<3;i++)
  {
      for(j=0;j<3;j++)
      {
          printf("[%d][%d]=",i+1, j+1);
          scanf("%d", &A[i][j]);
      }
  }
      printf("\n\n");
  printf("Escribe el valor de la matriz B\n");
  for(i=0;i<3;i++)
  {
      for(j=0;j<3;j++)
      {
          printf("[%d][%d]=", i+1, j+1);
          scanf("%d", &B[i][j]);
      }
  }
  for(i=0;i<3;i++)
  {
      for(j=0;j<3;j++)
      {
          C[i][j]=0;
          for(k=0;k<3;k++)
          {
              C[i][j]=(C[i][j]+(A[i][k]*B[k][j]));
          }
      }
  }
 
/*Rutina para imprimir*/
  printf("\n\n\t\t\t Matriz A");
  printf("\n");
  for(i=0;i<3;i++)
  {
      printf("\n\t\t");
      for(j=0;j<3;j++)
      {
          printf("  %3d  ", A[i][j]);
      }
  }
  printf("\n\n\t\t\t Matriz B");
  printf("\n");
  for(i=0;i<3;i++)
  {
      printf("\n\t\t");
      for(j=0;j<3;j++)
      {
          printf("  %3d  ", B[i][j]);
      }
  }
 
  printf("\n\n\t\t\t Matriz C");
  printf("\n");
  for(i=0;i<3;i++)
  {
      printf("\n\t\t");
      for(j=0;j<3;j++)
      {
          printf("  %3d  ", C[i][j]);
      }
  }
  printf("\n");
  system("PAUSE");	
  return 0;
}
 