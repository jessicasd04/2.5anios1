#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int serie(int,int);

int main(int argc, char *argv[]) {
	
	int n;
	printf("Introduce la cantidad de numeros de la serie:  ");
	scanf("%d",&n);
	serie(n,1);
	return 0;
}

int serie(int n,int i){
	int a,b;
	if(i!=(n+1)){
		if(i%2==0){
		a=i+2;
		b=i;
		printf("-%d/%d ",a,b);
		i++;
		serie(n,i);	
		}
		else{
			a=i+2;
	    	b=i;
	    	printf("%d/%d ",a,b);
	    	i++;
	    	serie(n,i);
		}
		
	}
}
