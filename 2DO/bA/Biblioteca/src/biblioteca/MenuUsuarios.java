package biblioteca;

import java.awt.Color;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;



public class MenuUsuarios extends JFrame{
            JButton b1,b2,b3,b4;
            JPanel panel, panel2;
            ImageIcon imagen;
            TextArea cuadro;
            TextField nombre, edad, direccion;
            JLabel t1,t2,t3;
            File archivo = new File("docUsuarios.txt");
            JFileChooser docUsuarios = new JFileChooser();
            FileReader leer;
            FileWriter escribir;
            Biblioteca Bio;
            Usuario user;
            ArrayList<Usuario> Usuarios = new ArrayList<>();
            ArrayList<Libros> Libros = new ArrayList<>(); 
            
            
            public MenuUsuarios(){
                     this.setSize(1000, 1000);
                    this.setTitle("La Biblioteca");
                    this.setResizable(false);
                    setLocationRelativeTo(null);
                    this.setLayout(null);
                    IniciarComponentes();
                    Implementar();
                    setDefaultCloseOperation(EXIT_ON_CLOSE);   
            }
            
            public void IniciarComponentes(){
                
                panel = new JPanel();
                panel.setLayout(null);
                panel.setBackground(Color.BLACK);    
                panel.setBounds(0, 0, 9999, 9999);
                this.getContentPane().add(panel);
       
                
                b1 = new JButton("Alta Usuario.");
                b2 = new JButton("Baja Usuario");
                b3 = new JButton("Modificación Usuario");
                b4 = new JButton("Lista");
                        
                b1.setFont(new java.awt.Font("Comic Sans MS",1,15));
                b1.setBounds(30, 160, 180, 40);
                panel.add(b1); 
                
                b2.setFont(new java.awt.Font("Comic Sans MS",1,15));
                b2.setBounds(30, 210, 180, 40);
                panel.add(b2);   
                
                b3.setFont(new java.awt.Font("Comic Sans MS",1,12));
                b3.setBounds(30, 260, 180, 40);
                panel.add(b3);   
                
                b4.setFont(new java.awt.Font("Comic Sans MS",1,15));
                b4.setBounds(30, 310, 180, 40);
                panel.add(b4);   
                
            }
            
            public void Implementar(){
            b1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                            
                           /*panel2 = new JPanel();
                           panel2.setLayout(null);
                           panel2.setBackground(Color.BLACK);    
                           panel2.setBounds(400, 0, 9999, 9999);
                           panel.add(panel2);*/
                    
                           t1 = new JLabel("Nombre: ");
                           t1.setForeground(Color.white);
                           t1.setFont(new java.awt.Font("Cooper Black",1,15));
                           t1.setBounds(350, 170, 100, 20);
                           panel.add(t1);
                           
                           t2 = new JLabel("Edad: ");
                           t2.setForeground(Color.white);
                           t2.setFont(new java.awt.Font("Cooper Black",1,15));
                           t2.setBounds(350, 250, 100, 20);
                           panel.add(t2);
                           
                           t3 = new JLabel("Direccion: ");
                           t3.setForeground(Color.white);
                           t3.setFont(new java.awt.Font("Cooper Black",1,15));
                           t3.setBounds(350, 330, 100, 20);
                           panel.add(t3);
                           
                           this.add(nombre= new TextField());
                           nombre.setFont(new java.awt.Font("Comic Sans MS",1,15));
                           nombre.setBounds(350, 210, 280, 20);
                           nombre.setFocusable(true);
                           panel.add(nombre);
                           
                           this.add(edad= new TextField());
                           edad.setFont(new java.awt.Font("Comic Sans MS",1,15));
                           edad.setBounds(350, 290, 280, 20);
                           edad.setFocusable(true);
                           panel.add(edad);
                           
                           this.add(direccion= new TextField());
                           direccion.setFont(new java.awt.Font("Comic Sans MS",1,15));
                           direccion.setBounds(350, 370, 280, 20);
                           direccion.setFocusable(true);
                           panel.add(direccion);
                           
                            JButton guardar;
                            guardar = new JButton("Guardar.");
                            guardar.setFont(new java.awt.Font("Comic Sans MS",1,15));
                            guardar.setBounds(500, 500, 150, 30);
                            panel.add(guardar);
                            
                            guardar.addActionListener(this);
                            
                            guardar.addActionListener(new ActionListener(){
                            
                                @Override
                                public void actionPerformed(ActionEvent ae) {
                                                  GuardarUsuario();    
                                
                                } 

                                });
                }
                
                private void add(TextField textField) {

                }          
            });
            this.dispose();
            b2.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent ae) {
                           t1 = new JLabel("Buscar Nombre: ");
                           t1.setForeground(Color.white);
                           t1.setFont(new java.awt.Font("Cooper Black",1,15));
                           t1.setBounds(350, 170, 100, 20);
                           panel.add(t1);
                           
                           this.add(nombre= new TextField());
                           nombre.setFont(new java.awt.Font("Comic Sans MS",1,15));
                           nombre.setBounds(350, 210, 280, 20);
                           nombre.setFocusable(true);
                           panel.add(nombre);
                           
                }

                private void add(TextField textField) {

                }
                
            });
            this.dispose();
           b3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
               
                }
            });
           this.dispose();
            b4.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent ae) {
                            
                           JButton mostrar;
                            mostrar = new JButton("Mostrar.");
                            mostrar.setFont(new java.awt.Font("Comic Sans MS",1,15));
                            mostrar.setBounds(500, 500, 150, 30);
                            panel.add(mostrar);                    
                            
                            JTextArea cuadro;
                            cuadro = new JTextArea();
                            cuadro.setBackground(Color.white);
                            cuadro.setBounds(350, 170 , 500, 300);
                            cuadro.setForeground(Color.black);
                            panel.add(cuadro);
                            cuadro.setVisible(true);
                            mostrar.addActionListener(this);
                            
                            mostrar.addActionListener(new ActionListener(){
                            
                                @Override
                                public void actionPerformed(ActionEvent ae) {
                                                  MostrarUsuario();    
                                
                                }
            });
                }
                
            });
            this.dispose();             
            
 
            
            }
            
 protected boolean GuardarUsuario() {
        //docUsuarios.showSaveDialog(this);
        //archivo = docUsuarios.getSelectedFile();
        //if(archivo != null){
            
            try{                       
                File archivo = new File("docUsuarios.txt");
                BufferedWriter bw;
                if(archivo.exists()){
                escribir = new FileWriter(archivo,true);
                bw = new BufferedWriter(escribir);    
                bw.newLine();
                bw.write(nombre.getText()+"\t"+edad.getText()+"\t"+direccion.getText());

                } else{
                escribir = new FileWriter(archivo.getPath());
                bw = new BufferedWriter(escribir);                  
                bw.write(nombre.getText()+"\t"+edad.getText()+"\t"+direccion.getText());                 
                }                
                bw.close();
                escribir.close();

                    }
            catch (IOException ex){
                
            }
            return true;
 }   
 
    protected boolean MostrarUsuario(){
        try{
            File archivo = new File("docUsuarios.txt");; 
            BufferedReader br;
            if(archivo.exists()){
                leer = new FileReader(archivo);
                br = new BufferedReader(leer);
                String linea;
                while((linea = br.readLine()) != null){
                    cuadro.append(linea+"\n");
                    linea = br.readLine();
                }
            } else{
                JOptionPane.showInputDialog("El archivo no existe");
            }
        }
        catch (Exception ex){
            
        }
        return true;
    }
}
