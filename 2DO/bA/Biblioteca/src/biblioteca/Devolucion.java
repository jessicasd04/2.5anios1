package biblioteca;

import java.awt.Color;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Devolucion extends JFrame{
        TextArea cuadro, cuadro2;
        JButton regresar,guardar,buscar;
        TextField nombre, libro;
        JLabel texto1, texto2, texto3, texto4, texto5, texto6, texto7, texto8;
        JPanel panel;
        JComboBox dia, mes, año;
        boolean bandera = false;
        
        public Devolucion(){
            this.setSize(800, 600);
            this.setTitle("Biblioteca");
            this.setResizable(false);
            setLocationRelativeTo(null);
            this.setLayout(null);
            IniciarComponentes();
            Implementar();
            setDefaultCloseOperation(EXIT_ON_CLOSE);  
        }
        
        public void IniciarComponentes(){
            panel = new JPanel();
            panel.setLayout(null);
            panel.setBackground(Color.CYAN);    
            panel.setBounds(0, 0, 9999, 9999);
            this.getContentPane().add(panel);
            
            texto6 = new JLabel("Lista Prestamos");
            texto6.setForeground(Color.magenta);
            texto6.setFont(new java.awt.Font("Cooper Black",1,15));
            texto6.setBounds(380, 50, 200, 20);
            panel.add(texto6);
            
            cuadro = new TextArea();
            cuadro.setBackground(Color.white);
            cuadro.setBounds(350, 80, 430, 180);
            cuadro.setForeground(Color.black);
            cuadro.setEnabled(false);
            panel.add(cuadro);
            
            texto7 = new JLabel("Lista Devoluciones");
            texto7.setForeground(Color.magenta);
            texto7.setFont(new java.awt.Font("Cooper Black",1,15));
            texto7.setBounds(380, 280, 200, 20);
            panel.add(texto7);
            
            cuadro2 = new TextArea();
            cuadro2.setBackground(Color.white);
            cuadro2.setBounds(350, 310, 430, 180);
            cuadro2.setForeground(Color.black);
            cuadro2.setEnabled(false);
            panel.add(cuadro2);
            
             texto1 = new JLabel("Ingresar nombre: ");
             texto1.setForeground(Color.magenta);
             texto1.setFont(new java.awt.Font("Cooper Black",1,15));
             texto1.setBounds(50, 50, 200, 20);
             panel.add(texto1);
            
             this.add(nombre = new TextField());
             nombre.setFont(new java.awt.Font("Comic Sans MS",1,15));
             nombre.setBounds(40, 90, 280, 20);
             nombre.setFocusable(true);
             panel.add(nombre);
             
             buscar = new JButton("Buscar");
             buscar.setFont(new java.awt.Font("Comic Sans MS",1,15));
             buscar.setBounds(200, 120, 100, 30);
             panel.add(buscar);
             
             texto2 = new JLabel("Ingresar Libro: ");
             texto2.setForeground(Color.magenta);
             texto2.setFont(new java.awt.Font("Cooper Black",1,15));
             texto2.setBounds(50, 180, 200, 20);
             panel.add(texto2);
             
             this.add(libro = new TextField());
             libro.setFont(new java.awt.Font("Comic Sans MS",1,15));
             libro.setBounds(40, 220, 280, 20);
             libro.setFocusable(true);
             panel.add(libro);
             
              dia  = new JComboBox();
              mes = new JComboBox();
              año = new JComboBox();
              
              for(int i = 1; i <= 31; i++){
                dia.addItem(i);
                }
            for(int i = 1; i <= 12; i++){
                mes.addItem(i);
            }
            for(int i = 1930; i <=2030; i++ ){
                año.addItem(i);
            }
            
             texto3 = new JLabel("Fecha de devolución: ");
             texto3.setForeground(Color.magenta);
             texto3.setFont(new java.awt.Font("Cooper Black",1,20));
             texto3.setBounds(50, 270, 250, 30);
             panel.add(texto3);
            
             texto4 = new JLabel("Día:");
             texto4.setForeground(Color.magenta);
             texto4.setFont(new java.awt.Font("Cooper Black",1,10));
             texto4.setBounds(50, 330, 50, 10);
             panel.add(texto4);
             
             dia.setBounds(50,350, 50, 20);
            panel.add(dia);
                        
             texto5 = new JLabel("Mes:");
             texto5.setForeground(Color.magenta);
             texto5.setFont(new java.awt.Font("Cooper Black",1,10));
             texto5.setBounds(130, 330, 50, 10);
             panel.add(texto5);
            
             mes.setBounds(130,350, 50, 20);
            panel.add(mes);
                        
             texto6 = new JLabel("Año:");
             texto6.setForeground(Color.magenta);
             texto6.setFont(new java.awt.Font("Cooper Black",1,10));
             texto6.setBounds(210, 330, 50, 10);
             panel.add(texto6);
             
            año.setBounds(210,350, 100, 20);
            panel.add(año);

            regresar = new JButton("Regresar");
            regresar.setFont(new java.awt.Font("Comic Sans MS",1,15));
            regresar.setBounds(60, 400, 100, 30);
            panel.add(regresar);
            
            guardar = new JButton("Guardar.");
            guardar.setFont(new java.awt.Font("Comic Sans MS",1,15));
            guardar.setBounds(200, 400, 100, 30);
            guardar.setEnabled(false);
            panel.add(guardar);
        }
        
        /*public void Auxiliar(){
            if(bandera = true){
                guardar.setEnabled(true);
                cuadro.setEnabled(true);
            }
        }*/
        
    public void MostrarListado(){
     String linea;       
     FileReader leer;
     File archivo;
     BufferedReader br;
     
    try{    
        archivo = new File("docPrestamos.txt");
        leer = new FileReader(archivo);           
        br = new BufferedReader(leer);  
        boolean verdad = false;
        while((linea = br.readLine()) != null){
               String persona [] = linea.split("\t");
               String Nusuarios = nombre.getText();
                    if(persona[0].equals(Nusuarios)){
                    verdad = true;
                    bandera = true;
                    Usuario u = new Usuario(persona[0],persona[1],persona[2]);
                    cuadro.append(linea+"\n");
                    br.read();
                    }
        }
        if(verdad == true){
                 JOptionPane.showMessageDialog(this,"Persona Encontrada");
        }
        if(verdad == false){
            JOptionPane.showMessageDialog(this,"Persona no Encontrada");
        }
          br.close();
          leer.close();
        }
        catch (Exception ex){
            JOptionPane.showMessageDialog(this,"Archivo no existe");
        }
}

    public void Auxiliar(){
        if(bandera == true){
           guardar.setEnabled(true);
        }
    }
        
       public void MostrarDevoluciones(){
       String linea;        
       FileReader leer;
       File archivo;
       BufferedReader br;
       
         try{    

        archivo = new File("docDevoluciones.txt");
        leer = new FileReader(archivo);           
        br = new BufferedReader(leer);
          while((linea = br.readLine()) !=null){
                              cuadro2.append(linea+"\n");                
                }
          br.close();
          leer.close();
        }
        catch (Exception ex){
            JOptionPane.showMessageDialog(this,"Archivo no existe");
        }
    } 
       
        public void Implementar(){
            buscar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                     MostrarListado();
                     String Nusuario  = nombre.getText();
                     Usuario u = new Usuario();
                     Auxiliar();
                }
            });
        
             guardar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                     String Nusuario  = nombre.getText();
                     String TLibro = libro.getText();
                     String Dia = dia.getSelectedItem().toString();
                     String Mes = mes.getSelectedItem().toString();
                     String Año = año.getSelectedItem().toString();
                     Usuario u = new Usuario(Nusuario);
                     Libros l = new Libros(TLibro);
                     libreria lib = new libreria(u,l, Dia, Mes, Año);
                     lib.GuardarListaDevoluciones();
                     MostrarDevoluciones();
                     lib.EliminarPrestamo();    
                }      
            });
     
           regresar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                     MenuPrincipal mp = new MenuPrincipal();
                     mp.setVisible(true);
                     dispose();
                }
            });
        }

    }
        

