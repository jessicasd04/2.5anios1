
package biblioteca;

import java.awt.Color;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class EliminarLibros extends JFrame{
    JPanel panel;
    JLabel t1, t2;
    TextField titulo ,eliminar;
    JButton buscar, borrar, regresar;
    TextArea cuadro;
    
    public EliminarLibros(){
                    this.setSize(700, 250);
                    this.setTitle("Biblioteca");
                    this.setResizable(false);
                    setLocationRelativeTo(null);
                    this.setLayout(null);
                    IniciarComponentes();
                    Implementar();
                    setDefaultCloseOperation(EXIT_ON_CLOSE);   
    }
    public void IniciarComponentes(){
        panel = new JPanel();
            panel.setLayout(null);
            panel.setBackground(Color.white);    
            panel.setBounds(0, 0, 9999, 9999);
            this.getContentPane().add(panel);
            
            t1 = new JLabel("Buscar Libro:");
            t1.setForeground(Color.black);
            t1.setFont(new java.awt.Font("Cooper Black",1,15));
            t1.setBounds(30, 30, 200, 20);
            panel.add(t1);
            
            this.add(titulo = new TextField());
            titulo.setFont(new java.awt.Font("Comic Sans MS",1,15));
            titulo.setBounds(20, 70, 280, 20);
            titulo.setFocusable(true);
            panel.add(titulo);
            
            buscar = new JButton("Buscar");
            buscar.setFont(new java.awt.Font("Comic Sans MS",1,15));
            buscar.setBounds(170, 110, 100, 30);
            panel.add(buscar);  
            
            cuadro = new TextArea();
            cuadro.setBackground(Color.white);
            cuadro.setBounds(320, 10 , 370, 200);
            cuadro.setForeground(Color.black);
            cuadro.setEnabled(false);
            panel.add(cuadro);            
            
            borrar = new JButton("Eliminar");
            borrar.setFont(new java.awt.Font("Comic Sans MS",1,15));
            borrar.setBounds(40, 110, 100, 30);
            borrar.setEnabled(false);
            panel.add(borrar);  
            
            regresar = new JButton("Regresar");
            regresar.setFont(new java.awt.Font("Comic Sans MS",1,15));
            regresar.setBounds(30, 170, 150, 30);
            panel.add(regresar);
        
    }
public void MostrarLibros(){
       
    String linea;
    File archivo;
    FileReader leer;
    BufferedReader br;
    
         try{    
        archivo = new File("docLibros.txt");
        leer = new FileReader(archivo);           
        br = new BufferedReader(leer);
        while((linea = br.readLine()) != null){
                    cuadro.append(linea+"\n");         
          }
          br.close();
          leer.close();
        }
        catch (Exception ex){
            JOptionPane.showMessageDialog(this,"No existe el archivo :C");
        }
    }
    

    public void Implementar(){
        buscar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                     String Tlibro = titulo.getText();
                     Libros l = new Libros(Tlibro);
                     l.BuscarLibro();
                      if(l.bandera == true){
                           borrar.setEnabled(true);
                        }
                }
            });
        
                borrar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                        String Tlibro = titulo.getText();
                        Libros l = new Libros(Tlibro);
                        l.EliminarLibros();
                        MostrarLibros();
                }
            });
                
                regresar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                     MenuPrincipal mp = new MenuPrincipal();
                     mp.setVisible(true);
                     dispose();
                }
            });
    }
}
