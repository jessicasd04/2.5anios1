package biblioteca;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;



public class VentanaPrincipal extends JFrame implements ActionListener{      
            JPanel panel;
            JLabel texto1;
            JLabel texto2;
            TextField usuario, contraseña;
            ImageIcon imagen,imagen2;
            JButton boton;
            private Component comfirmacion;
            private String user,password;
 
            
            public VentanaPrincipal(){
                    this.setSize(350, 350);
                    this.setTitle("Biblioteca");
                    this.setResizable(false);
                    setLocationRelativeTo(null);
                    IniciarComponentes();                   
                   // this.repaint();
                    setDefaultCloseOperation(EXIT_ON_CLOSE);
            }            
       
            public void IniciarComponentes(){
               imagen = new ImageIcon(this.getClass().getResource("libros.jpg"));
               imagen2 = new ImageIcon(this.getClass().getResource("Boton-Ingresar.png"));
               
               panel = new JPanel();
                panel.setLayout(null);
                panel.setBackground(Color.black);    
                panel.setOpaque(true);
                panel.setBounds(0, 0, 9999, 9999);
               this.getContentPane().add(panel);
                              
                texto1 = new JLabel("Usuario: ");
                texto1.setForeground(Color.white);
                texto1.setFont(new java.awt.Font("Cooper Black",1,15));
                texto1.setBounds(40, 130, 100, 20);
                panel.add(texto1);
                
                texto2 = new JLabel("Contraseña: ");
                texto2.setForeground(Color.white);
                texto2.setFont(new java.awt.Font("Cooper Black",1,15));
                texto2.setBounds(40, 190, 150, 20);
                panel.add(texto2);
                
                this.add(usuario= new TextField());
                usuario.setFont(new java.awt.Font("Comic Sans MS",1,15));
                usuario.setBounds(30, 160, 280, 20);
                usuario.setFocusable(true);
                panel.add(usuario);
                
                this.add(contraseña= new TextField());
                contraseña.setFont(new java.awt.Font("Comic Sans MS",1,15));
                contraseña.setBounds(30, 220, 280, 20);
                contraseña.setFocusable(true);
                panel.add(contraseña);                
                
                boton = new JButton("Entrar");
                boton.setFont(new java.awt.Font("Comic Sans MS",1,15));
                boton.setBounds(210, 260, 120, 30);
                panel.add(boton);
               boton.addActionListener(this);
            }

            public void datos(String us, String pass){
                user = "Admin";
                password = "admin";
            }     
        
            @Override
        public void paint(Graphics G){
            G.drawImage(imagen.getImage(),0,0,350,350,null);
            G.drawImage(imagen2.getImage(),210, 270,120,60,null);
        }             
            
            @Override
           public void actionPerformed(ActionEvent ae) {
                 datos(user,password);
                 if(user.equals(usuario.getText()) && password.equals(contraseña.getText())){
                     MenuPrincipal mp = new MenuPrincipal();
                     mp.setVisible(true);
                     this.dispose();
                 }else if(usuario.getText().equals("") || contraseña.getText().equals("")){
                     JOptionPane.showMessageDialog(this,"Usuario y/o Contraseña están vacíos");
                     usuario.setFocusable(true);
                 }else if(usuario.getText().compareTo(user) != 0 || contraseña.getText().compareTo(password ) != 0){
                     JOptionPane.showMessageDialog(this, "Usuario y/o Contraseña erróneos.\nIngrese nuevamente");
                     usuario.setFocusable(true);
                 }
                 
             }
    }