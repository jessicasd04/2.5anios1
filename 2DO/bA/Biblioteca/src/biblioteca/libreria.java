package biblioteca;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;

public class libreria extends PrestarLibro{
    public Usuario usuarios = new Usuario();
    public Libros libros = new Libros();
    String diaE, mesE, añoE;
    String diaD, mesD, añoD;
    public libreria() {
        
    }
    
    public libreria(Usuario usuarios, Libros libros, String diaE, String mesE, String añoE){
        this.usuarios = usuarios;
        this.libros = libros;
        this.diaE = diaE;
        this.mesE = mesE;
        this.añoE = añoE;
    }
    
    public libreria(Usuario usuarios, Libros libros, String diaE, String mesE, String añoE, String diaD, String mesD, String añoD){
        this.usuarios = usuarios;
        this.libros = libros;
        this.diaE = diaE;
        this.diaD = diaD;
        this.mesE = mesE;
        this.mesD = mesD;
        this.añoD = añoD;
        this.añoE = añoE;
    }

    libreria(String nombre, String titulo, String Dia, String Mes, String Año) {
            //this.usuarios.nombre = nombre;
            //this.libros.titulo = titulo;
            //this.diaE = Dia;
    }

    public Usuario getUsuarios() {
        return usuarios;
    }

    public Usuario setUsuarios() {
                return usuarios;
    }

    public Libros getLibros() {
        return libros;
    }

    public void setLibros(Libros libros) {
        this.libros = libros;
    }
    
    public void GuardarListaPrestamos(){
          try{                       
                File archivo = new File("docPrestamos.txt");
                BufferedWriter bw;
                FileWriter escribir;
                if(archivo.exists() && archivo.length() != 0){
                escribir = new FileWriter(archivo,true);
                bw = new BufferedWriter(escribir);    
                bw.newLine();
                bw.write(usuarios.nombre+"\t"+usuarios.edad+"\t"+libros.titulo+"\t"+libros.editorial+"\t"+libros.NumEjemplar+"\t"+diaE+"\t"+mesE+"\t"+añoE+"\t"+diaD+"\t"+mesD+"\t"+añoD);
                JOptionPane.showMessageDialog(this,"Se ha realizado el registro con éxito :3");
                } 
                else{
                escribir = new FileWriter(archivo);
                bw = new BufferedWriter(escribir);
                bw.write(usuarios.nombre+"\t"+usuarios.edad+"\t"+libros.titulo+"\t"+libros.editorial+"\t"+libros.NumEjemplar+"\t"+diaE+"\t"+mesE+"\t"+añoE+"\t"+diaD+"\t"+mesD+"\t"+añoD);
                JOptionPane.showMessageDialog(this,"Se ha realizado el registro con éxito :3");
                }                
                bw.close();
                escribir.close();
                    }
            catch (IOException ex){
                JOptionPane.showMessageDialog(this,"Archivo no encontrado");
            }
    }
    
  public void GuardarListaDevoluciones(){
        
      try{
                File archivo = new File("docDevoluciones.txt");
                BufferedWriter bw;
                FileWriter escribir;
                if(archivo.exists() && archivo.length() != 0){
                escribir = new FileWriter(archivo,true);
                bw = new BufferedWriter(escribir);    
                bw.newLine();
                bw.write(usuarios.nombre+"\t"+libros.titulo+"\t"+diaE+"\t"+mesE+"\t"+añoE);
                JOptionPane.showMessageDialog(this,"Se ha devuelto el libro exitosamente :3");
                } 
                else{
                escribir = new FileWriter(archivo);
                bw = new BufferedWriter(escribir);
                bw.write(usuarios.nombre+"\t"+libros.titulo+"\t"+diaE+"\t"+mesE+"\t"+añoE);
                JOptionPane.showMessageDialog(this,"Se ha devuelto el libro exitosamente :3");
                }                
                bw.close();
                escribir.close();
      }
            catch (IOException ex){
                JOptionPane.showMessageDialog(this,"Archivo no encontrado");
            }
    }
  
   public void EliminarPrestamo(){
        try{
            File archivo = new File("docPrestamos.txt");
            if(archivo.exists()){
                FileReader fr = new FileReader(archivo);
                BufferedReader br = new BufferedReader(fr);
                String linea;
                int NumLineas = 0;
                while((linea = br.readLine()) != null){
                    NumLineas ++;    
                }
                String persona [] = new String[NumLineas];
                br = new BufferedReader(new FileReader(archivo));
                int i = 0;
                while((linea = br.readLine()) != null){
                    persona [i] = linea;             
                    i++;
                }
                FileWriter fw = new FileWriter(archivo);
                BufferedWriter bw = new BufferedWriter(fw);
                boolean verdad = false;
                boolean verdad2 = true;
                for(int j = 0; j<persona.length; j++){
                    String Linea [] = persona[j].split("\t");
                    if(Linea[0].equals(usuarios.nombre)){
                        verdad = true;
                        //JOptionPane.showMessageDialog(this,"Persona eliminada");
                    }
                    else{
                        if(verdad2 == true){
                        bw.write(persona[j]);
                        verdad2 = false;
                        }
                        else{
                            bw.newLine();
                            bw.write(persona[j]);
                        } 
                    }
                }
                if(verdad == false){
                    //JOptionPane.showMessageDialog(this,"Persona no fue encontrada");
                }
                if(NumLineas == 1 && verdad == true){
                    archivo.delete();
                }
                
                bw.close();
                    fw.close();
            }
        }
        catch(Exception ex){
            JOptionPane.showMessageDialog(this,"Archivo inexistente");
        }
    } 
}

    
    

