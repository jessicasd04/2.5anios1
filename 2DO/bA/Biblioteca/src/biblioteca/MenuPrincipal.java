package biblioteca;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.Timer;



public class MenuPrincipal extends JFrame implements ActionListener{
            JPanel panel;
            JMenu MenuUsuarios;
            JMenu MenuLibros;
            JMenu Prestamos;
            JMenu Devoluciones;
            JMenu Salir;
            JMenuBar barra = new JMenuBar();
            JMenuItem AgregarUsuario = new JMenuItem("Agregar Usuario");
            JMenuItem EliminarUsuario = new JMenuItem("Eliminar Usuario");
            JMenuItem ModificarUsuario = new JMenuItem("Modificar Usuario");
            JMenuItem ListaUsuario = new JMenuItem("Lista Usuarios");
            JMenuItem AgregarLibro = new JMenuItem("Agregar Libro");
            JMenuItem EliminarLibro = new JMenuItem("Eliminar Libro");
            JMenuItem ModificarLibro = new JMenuItem("Modificar Libro");
            JMenuItem ListaLibro = new JMenuItem("Lista Libros");
            JMenuItem AgregarPrestamo = new JMenuItem("Agregar Prestamo");
            JMenuItem DevolverLibro = new JMenuItem("Devolver Libro");
            JMenuItem SalirSistema = new JMenuItem("Salir");
            JLabel fondo;
            Lianza T;
            Timer tiempo;
            int x = 0;
            int y = 0;
            public MenuPrincipal(){
                
                    this.setSize(500, 500);
                    this.setTitle("Biblioteca");
                    this.setResizable(false);
                    setLocationRelativeTo(null);
                    //this.setLayout(null);
                    IniciarComponentes();                    
                    Implementar();
                    setDefaultCloseOperation(EXIT_ON_CLOSE); 
            }
            
           public void IniciarComponentes(){
                
               T = new Lianza();
               T.Init();               
               tiempo = new Timer(1000,this);
               tiempo.start();
               add(T);


//this.getContentPane().add(T);
                //panel = new JPanel();
                //T.setLayout(null);
                //T.setBackground(Color.white); 
                
                //this.getContentPane().add(panel);
                
                MenuUsuarios = new JMenu("Usuarios");
                MenuUsuarios.add(AgregarUsuario);
                MenuUsuarios.add(EliminarUsuario);
               // MenuUsuarios.add(ModificarUsuario);
                MenuUsuarios.add(ListaUsuario);
                MenuLibros = new JMenu("Libros");
                MenuLibros.add(AgregarLibro);
                MenuLibros.add(EliminarLibro);
                //MenuLibros.add(ModificarLibro);
                MenuLibros.add(ListaLibro);
                Prestamos = new JMenu("Prestamos");
                Prestamos.add(AgregarPrestamo);
                Devoluciones = new JMenu("Devoluciones");
                Devoluciones.add(DevolverLibro);
                Salir = new JMenu("Opciones");
                Salir.add(SalirSistema);
                        
                barra.add(MenuUsuarios);
                barra.add(MenuLibros);
                barra.add(Prestamos);
                barra.add(Devoluciones);
                barra.add(Salir);
                T.add(barra);
                
                //fondo = new JLabel(new ImageIcon("libros.jpg"));
                //fondo.setBounds(0, 20,200, 200);
                //panel.add(fondo);
                
                setJMenuBar(barra);
            }
           
           public void Implementar(){
               AgregarUsuario.addActionListener(new ActionListener(){
                   @Override
                   public void actionPerformed(ActionEvent ae) {
                       
                        AgregarUsuario au = new AgregarUsuario();
                        au.setVisible(true);  
                        dispose();
                   }
               });
               AgregarLibro.addActionListener(new ActionListener(){
                   @Override
                   public void actionPerformed(ActionEvent ae) {
                        AgregarLibro al = new AgregarLibro();
                        al.setVisible(true);  
                        dispose();
                   }
               });
           ListaUsuario.addActionListener(new ActionListener(){
                   @Override
                   public void actionPerformed(ActionEvent ae) {
                   ListarUsuarios lu = new ListarUsuarios();
                   lu.setVisible(true);
                        dispose();
                   }
               });
           ListaLibro.addActionListener(new ActionListener(){
                   @Override
                   public void actionPerformed(ActionEvent ae) {
                   ListarLibros ll = new ListarLibros();
                   ll.setVisible(true);
                        dispose();
                   }
               });
            EliminarUsuario.addActionListener(new ActionListener(){
                   @Override
                   public void actionPerformed(ActionEvent ae) {
                   Eliminarusuario eu = new Eliminarusuario();
                   eu.setVisible(true);
                        dispose();
                   }
               });
             EliminarLibro.addActionListener(new ActionListener(){
                   @Override
                   public void actionPerformed(ActionEvent ae) {
                   EliminarLibros el = new EliminarLibros();
                   el.setVisible(true);
                        dispose();
                   }
               });
            
               AgregarPrestamo.addActionListener(new ActionListener(){
                   @Override
                   public void actionPerformed(ActionEvent ae) {
                   PrestarLibro pl = new PrestarLibro();
                   pl.setVisible(true);
                        dispose();
                   }
               });
             DevolverLibro.addActionListener(new ActionListener(){
                   @Override
                   public void actionPerformed(ActionEvent ae) {
                   Devolucion dev = new Devolucion();
                   dev.setVisible(true);
                   dispose();
                   }
               });
             
               SalirSistema.addActionListener(new ActionListener(){
                   @Override
                   public void actionPerformed(ActionEvent ae) {
                         System.exit(0);
                   }
               });
           }

    @Override
    public void actionPerformed(ActionEvent ae) {
                             
                            x=x-10;
                            y=y+2;
                            T.movimiento(x,y);
                            T.repaint();
    }
}