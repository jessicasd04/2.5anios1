package biblioteca;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;

public class Libros extends AgregarLibro {
        public String titulo;
        public String editorial;
        public String NumEjemplar;
        public boolean bandera = false;
        
        public Libros(){
            
        }
        
        public Libros(boolean bandera){
            this.bandera = bandera;
        }
        
        public Libros(String titulo){
            this.titulo = titulo;
        }
        
    public Libros(String titulo, String editorial, String NumEjemplar) {
        this.titulo = titulo;
        this.editorial = editorial;
        this.NumEjemplar = NumEjemplar;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public String getNumEjemplar() {
        return NumEjemplar;
    }

    public void setNumEjemplar(String NumEjemplar) {
        this.NumEjemplar = NumEjemplar;
    }
    
    public void GuardarLibro() {
            try{                       
                File archivo = new File("docLibros.txt");
                BufferedWriter bw;
                FileWriter escribir;
                if(archivo.exists()){
                escribir = new FileWriter(archivo,true);
                bw = new BufferedWriter(escribir);    
                bw.newLine();
                bw.write(titulo+"\t"+editorial+"\t"+NumEjemplar);
                 JOptionPane.showMessageDialog(this,"Libro Agregadao :3");
                } 
                else{
                escribir = new FileWriter(archivo.getPath());
                bw = new BufferedWriter(escribir);                  
                bw.write(titulo+"\t"+editorial+"\t"+NumEjemplar);
                JOptionPane.showMessageDialog(this,"Libro Agregado :3");
                }                
                bw.close();
                escribir.close();

                    }
            catch (IOException ex){
                JOptionPane.showMessageDialog(this,"No existe el Archivo :C");
            }
    
    }
    
        public void BuscarLibro(){
        try{
            File archivo = new File("docLibros.txt");
            FileReader leer;
            BufferedReader br;
            if(archivo.exists()){
                leer = new FileReader(archivo);
                br = new BufferedReader(leer);
                String linea;
                boolean verdad = false;
                while((linea = br.readLine()) != null){
                    String libros [] = linea.split("\t");
                    if(libros [0].equals(titulo)){
                    verdad = true;
                    bandera = true;
                    Libros l = new Libros(libros[0],libros[1],libros[2]);
                    JOptionPane.showMessageDialog(this,"Libro Encontrada");
                    br.read();
                    }
                }
                if(verdad == false){
                    JOptionPane.showMessageDialog(this,"Libro no Encontrada");
                }
            }            
        }
        catch(Exception ex){
            JOptionPane.showMessageDialog(this, "No existe el archivo :C");
        }       
    }
        
    public void EliminarLibros(){
        try{
            File archivo = new File("docLibros.txt");
            if(archivo.exists()){
                FileReader fr = new FileReader(archivo);
                BufferedReader br = new BufferedReader(fr);
                String linea;
                int NumLineas = 0;
                while((linea = br.readLine()) != null){
                    NumLineas ++;    
                }
                String persona [] = new String[NumLineas];
                br = new BufferedReader(new FileReader(archivo));
                int i = 0;
                while((linea = br.readLine()) != null){
                    persona [i] = linea;             
                    i++;
                }
                FileWriter fw = new FileWriter(archivo);
                BufferedWriter bw = new BufferedWriter(fw);
                boolean verdad = false;
                boolean verdad2 = true;
                for(int j = 0; j<persona.length; j++){
                    String Linea [] = persona[j].split("\t");
                    if(Linea[0].equals(titulo)){
                        verdad = true;
                        JOptionPane.showMessageDialog(this,"Libro eliminado");
                    }
                    else{
                        if(verdad2 == true){
                        bw.write(persona[j]);
                        verdad2 = false;
                        }
                        else{
                            bw.newLine();
                            bw.write(persona[j]);
                        } 
                    }
                }
                if(verdad == false){
                    JOptionPane.showMessageDialog(this,"Libro no encontrado");
                }
                if(NumLineas == 1 && verdad == true){
                    archivo.delete();
                }
                
                bw.close();
                    fw.close();
            }
        }
        catch(Exception ex){
            JOptionPane.showMessageDialog(this,"No existe el archivo :C");
        }
    } 
}
