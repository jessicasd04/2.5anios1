package biblioteca;

import java.awt.Color;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class ListarUsuarios extends JFrame{
        JPanel panel;
        TextArea cuadro;
        FileReader leer;
        BufferedReader br;
        File archivo;
        JButton mostrar, regresar;
        
    public ListarUsuarios(){
                    this.setSize(500, 400);
                    this.setTitle("Biblioteca");
                    this.setResizable(false);
                    setLocationRelativeTo(null);
                    this.setLayout(null);
                    IniciarComponentes();
                    Implementar();
                    setDefaultCloseOperation(EXIT_ON_CLOSE);  
    }
    public void IniciarComponentes(){
                        panel = new JPanel();
                        panel.setLayout(null);
                        panel.setBackground(Color.black);    
                        panel.setBounds(0, 0, 9999, 9999);
                        this.getContentPane().add(panel);
                        
                        cuadro = new TextArea();
                        cuadro.setBackground(Color.white);
                        cuadro.setBounds(5, 5 , 485, 250);
                        cuadro.setForeground(Color.black);
                        panel.add(cuadro);
 
                        regresar = new JButton("Regresar");
                        regresar.setFont(new java.awt.Font("Comic Sans MS",1,15));
                        regresar.setBounds(50, 300, 150, 30);
                        panel.add(regresar);
                        
                        mostrar = new JButton("Mostrar.");
                        mostrar.setFont(new java.awt.Font("Comic Sans MS",1,15));
                        mostrar.setBounds(300, 300, 150, 30);
                        panel.add(mostrar);  
    }

public void MostrarUsuario(){
       
    String linea;        
         try{    
        archivo = new File("docUsuarios.txt");
        leer = new FileReader(archivo);           
        br = new BufferedReader(leer);
        while((linea = br.readLine()) != null){
                    cuadro.append(linea+"\n");         
          }
          br.close();
          leer.close();
        }
        catch (Exception ex){
            
        }
    }
    public void Implementar(){
             
        regresar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                     MenuPrincipal mp = new MenuPrincipal();
                     mp.setVisible(true);
                     dispose();
                }
            });
                
            mostrar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                        MostrarUsuario();
                }
            });
    }
}
