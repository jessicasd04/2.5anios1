
package biblioteca;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.JOptionPane;


public class Usuario extends AgregarUsuario{
    
    Scanner entrada = new Scanner(System.in);
    String nombre;
    String edad;
    String direccion;
    boolean bandera = false;
    
   public Usuario(){
        
    }
   
   public Usuario(String nombre){
        this.nombre = nombre;
    }
   
   public Usuario(String nombre, String edad){
       this.nombre = nombre;
       this.edad = edad;
   }
    public Usuario(String nombre, String edad, String direccion) {
        this.nombre = nombre;
        this.edad = edad;
        this.direccion = direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    public void GuardarUsuario() {
            try{                       
                File archivo = new File("docUsuarios.txt");
                BufferedWriter bw;
                FileWriter escribir;
                if(archivo.exists() && archivo.length() != 0){
                escribir = new FileWriter(archivo,true);
                bw = new BufferedWriter(escribir);    
                bw.newLine();
                bw.write(nombre+"\t"+edad+"\t"+direccion);
                JOptionPane.showMessageDialog(this,"Persona agregada :3");
                } 
                else{
                escribir = new FileWriter(archivo);
                bw = new BufferedWriter(escribir);
                bw.write(nombre+"\t"+edad+"\t"+direccion);
                JOptionPane.showMessageDialog(this,"Persona agregada :3");
                }                
                bw.close();
                escribir.close();
                    }
            catch (IOException ex){
                JOptionPane.showMessageDialog(this,"Archivo no existe :C");
            }
 }
    
    public void BuscarUsuario(){
        try{
            File archivo = new File("docUsuarios.txt");
            FileReader leer;
            BufferedReader br;
            if(archivo.exists()){
                leer = new FileReader(archivo);
                br = new BufferedReader(leer);
                String linea;
                boolean verdad = false;
                while((linea = br.readLine()) != null){
                    String persona [] = linea.split("\t");
                    if(persona[0].equals(nombre)){
                    verdad = true;
                    bandera = true;
                    Usuario u = new Usuario(persona[0],persona[1],persona[2]);
                    JOptionPane.showMessageDialog(this,"Persona Encontrada");
                    br.read();
                    }
                }
                if(verdad == false){
                    JOptionPane.showMessageDialog(this,"Persona no Encontrada");
                }
            }            
        }
        catch(Exception ex){
            JOptionPane.showMessageDialog(this, "Archivo no existe :C");
        }
                
    }
    
    public void EliminarUsuario(){
        try{
            File archivo = new File("docUsuarios.txt");
            if(archivo.exists()){
                FileReader fr = new FileReader(archivo);
                BufferedReader br = new BufferedReader(fr);
                String linea;
                int NumLineas = 0;
                while((linea = br.readLine()) != null){
                    NumLineas ++;    
                }
                String persona [] = new String[NumLineas];
                br = new BufferedReader(new FileReader(archivo));
                int i = 0;
                while((linea = br.readLine()) != null){
                    persona [i] = linea;             
                    i++;
                }
                FileWriter fw = new FileWriter(archivo);
                BufferedWriter bw = new BufferedWriter(fw);
                boolean verdad = false;
                boolean verdad2 = true;
                for(int j = 0; j<persona.length; j++){
                    String Linea [] = persona[j].split("\t");
                    if(Linea[0].equals(nombre)){
                        verdad = true;
                        JOptionPane.showMessageDialog(this,"Persona eliminada");
                    }
                    else{
                        if(verdad2 == true){
                        bw.write(persona[j]);
                        verdad2 = false;
                        }
                        else{
                            bw.newLine();
                            bw.write(persona[j]);
                        } 
                    }
                }
                if(verdad == false){
                    JOptionPane.showMessageDialog(this,"Persona no fue encontrada");
                }
                if(NumLineas == 1 && verdad == true){
                    archivo.delete();
                }
                
                bw.close();
                    fw.close();
            }
        }
        catch(Exception ex){
            JOptionPane.showMessageDialog(this,"No existe el archivo :C");
        }
    }
}
