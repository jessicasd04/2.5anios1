package biblioteca;

import java.awt.Color;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class PrestarLibro extends JFrame{
                JPanel panel;
                TextArea cuadro, cuadro2, cuadro3;
                JLabel nombre, libro, lp, d, m, añ, fechaE, fechaD, dd, dm, añd, lu, ll, edad, editorial, numedicion;
                //JLabel titulo1, titulo2, titulo3, titulo4, titulo5, titulo6;
                JButton Buscar1, Buscar2, guardar, regresar;
                TextField t1,t2,t3,t4,t5;
                JComboBox dia, mes, año, diad, mesd, añod;
                File archivo;
                FileReader fr;
                BufferedReader br;
                boolean bandera1 = false;
                boolean bandera2 = false;
                
    public PrestarLibro(){
                    this.setSize(1200, 600);
                    this.setTitle("Biblioteca");
                    this.setResizable(false);
                    setLocationRelativeTo(null);
                    this.setLayout(null);
                    IniciarComponentes();
                    Implementar();
                    setDefaultCloseOperation(EXIT_ON_CLOSE); 
    }
    
    public void IniciarComponentes(){
            dia = new JComboBox();
            mes = new JComboBox();
            año = new JComboBox();
            diad = new JComboBox();
            mesd = new JComboBox();
            añod = new JComboBox();
            
            for(int i = 1; i <= 31; i++){
                dia.addItem(i);
                }
            for(int i = 1; i <= 12; i++){
                mes.addItem(i);
            }
            for(int i = 1930; i <=2030; i++ ){
                año.addItem(i);
            }
                        
            for(int i = 1; i <= 31; i++){
                diad.addItem(i);
            }
            for(int i = 1; i <= 12; i++){
                mesd.addItem(i);
            }
            for(int i = 1930; i <=2030; i++ ){
                añod.addItem(i);
            }
        
            panel = new JPanel();
            panel.setLayout(null);
            panel.setBackground(Color.yellow);    
            panel.setBounds(0, 0, 9999, 9999);
            this.getContentPane().add(panel);
            
            cuadro = new TextArea();
            cuadro.setBackground(Color.black);
            cuadro.setBounds(400, 190 , 380, 330);
            cuadro.setForeground(Color.white);
            cuadro.setEnabled(false);
            panel.add(cuadro);
            
            cuadro2 = new TextArea();
            cuadro2.setBackground(Color.black);
            cuadro2.setBounds(800,20,380,220);
            cuadro2.setForeground(Color.white);
            cuadro2.setEnabled(false);
            panel.add(cuadro2);

            ll = new JLabel("Lista Usuarios");
            ll.setForeground(Color.black);
            ll.setFont(new java.awt.Font("Cooper Black",1,15));
            ll.setBounds(940, 250, 160, 20);
            panel.add(ll);
            
            cuadro3 = new TextArea();
            cuadro3.setBackground(Color.white);
            cuadro3.setBounds(800, 300, 380, 220);
            cuadro3.setForeground(Color.black);
            cuadro3.setEnabled(false);
            panel.add(cuadro3);
            
            ll = new JLabel("Lista Libros");
            ll.setForeground(Color.black);
            ll.setFont(new java.awt.Font("Cooper Black",1,15));
            ll.setBounds(950, 530, 160, 20);
            panel.add(ll);
            
            lp = new JLabel("Lista Prestamos");
            lp.setForeground(Color.black);
            lp.setFont(new java.awt.Font("Cooper Black",1,15));
            lp.setBounds(520, 530, 160, 20);
            panel.add(lp);
            
            nombre = new JLabel("Nombre:");
            nombre.setForeground(Color.black);
            nombre.setFont(new java.awt.Font("Cooper Black",1,15));
            nombre.setBounds(35, 10, 100, 20);
            panel.add(nombre);
            
            this.add(t1= new TextField());
            t1.setFont(new java.awt.Font("Comic Sans MS",1,15));
            t1.setBounds(30, 50, 280, 20);
            t1.setFocusable(true);
            panel.add(t1);
            
            edad = new JLabel("Edad:");
            edad.setForeground(Color.black);
            edad.setFont(new java.awt.Font("Cooper Black",1,15));
            edad.setBounds(410, 10, 100, 20);
            panel.add(edad);
            
            this.add(t3= new TextField());
            t3.setFont(new java.awt.Font("Comic Sans MS",1,15));
            t3.setBounds(400, 50, 180, 20);
            t3.setFocusable(true);
            panel.add(t3);
            
            editorial = new JLabel("Editorial:");
            editorial.setForeground(Color.black);
            editorial.setFont(new java.awt.Font("Cooper Black",1,15));
            editorial.setBounds(410, 90, 200, 20);
            panel.add(editorial);
            
            this.add(t5= new TextField());
            t5.setFont(new java.awt.Font("Comic Sans MS",1,15));
            t5.setBounds(400, 130, 380, 20);
            t5.setFocusable(true);
            panel.add(t5);
            
            this.add(t4= new TextField());
            t4.setFont(new java.awt.Font("Comic Sans MS",1,15));
            t4.setBounds(600, 50, 180, 20);
            t4.setFocusable(true);
            panel.add(t4);
            
            numedicion = new JLabel("Num. de Edición:");
            numedicion.setForeground(Color.black);
            numedicion.setFont(new java.awt.Font("Cooper Black",1,15));
            numedicion.setBounds(600, 10, 200, 20);
            panel.add(numedicion);
            
       
            Buscar1 = new JButton("Buscar.");
            Buscar1.setFont(new java.awt.Font("Comic Sans MS",1,15));
            Buscar1.setBounds(180, 90, 100, 30);
            panel.add(Buscar1);  
            
            libro = new JLabel("Libro:");
            libro.setForeground(Color.black);
            libro.setFont(new java.awt.Font("Cooper Black",1,15));
            libro.setBounds(35, 140, 100, 20);
            panel.add(libro);           
            
            this.add(t2= new TextField());
            t2.setFont(new java.awt.Font("Comic Sans MS",1,15));
            t2.setBounds(30, 180, 280, 20);
            t2.setFocusable(true);
            panel.add(t2);
            
            Buscar2 = new JButton("Buscar.");
            Buscar2.setFont(new java.awt.Font("Comic Sans MS",1,15));
            Buscar2.setBounds(180, 220, 100, 30);
            panel.add(Buscar2);  
 
            fechaE = new JLabel("Fecha de entrega");
            fechaE.setForeground(Color.black);
            fechaE.setFont(new java.awt.Font("Cooper Black",1,20));
            fechaE.setBounds(80, 270, 300, 30);
            panel.add(fechaE);
            
            dd = new JLabel("Día");
            dd.setForeground(Color.black);
            dd.setFont(new java.awt.Font("Cooper Black",1,10));
            dd.setBounds(50, 310, 50, 20);
            panel.add(dd);
            
            dia.setBounds(50,340, 50, 20);
            panel.add(dia);
            
            dm = new JLabel("Mes");
            dm.setForeground(Color.black);
            dm.setFont(new java.awt.Font("Cooper Black",1,10));
            dm.setBounds(130, 310, 50, 20);
            panel.add(dm);
            
            mes.setBounds(130,340, 50, 20);
            panel.add(mes);
            
            añd = new JLabel("Año");
            añd.setForeground(Color.black);
            añd.setFont(new java.awt.Font("Cooper Black",1,10));
            añd.setBounds(210, 310, 50, 20);
            panel.add(añd);
            
            año.setBounds(210,340, 100, 20);
            panel.add(año);
            
            fechaD = new JLabel("Fecha de devolucion");
            fechaD.setForeground(Color.black);
            fechaD.setFont(new java.awt.Font("Cooper Black",1,20));
            fechaD.setBounds(65, 380, 300, 30);
            panel.add(fechaD);
          
            d = new JLabel("Día");
            d.setForeground(Color.black);
            d.setFont(new java.awt.Font("Cooper Black",1,10));
            d.setBounds(50, 430, 50, 20);
            panel.add(d);
            
            diad.setBounds(50,460, 50, 20);
            panel.add(diad);
            
            m = new JLabel("Mes");
            m.setForeground(Color.black);
            m.setFont(new java.awt.Font("Cooper Black",1,10));
            m.setBounds(130, 430, 50, 20);
            panel.add(m);
            
            mesd.setBounds(130,460, 50, 20);
            panel.add(mesd);
            
            añ = new JLabel("Año");
            añ.setForeground(Color.black);
            añ.setFont(new java.awt.Font("Cooper Black",1,10));
            añ.setBounds(210, 430, 50, 20);
            panel.add(añ);
            
            añod.setBounds(210,460, 100, 20);
            panel.add(añod);
            
            guardar = new JButton("Guardar.");
            guardar.setFont(new java.awt.Font("Comic Sans MS",1,15));
            guardar.setBounds(200, 500, 100, 30);
            guardar.setEnabled(false);
            panel.add(guardar);  
            
            regresar = new JButton("Regresar");
            regresar.setFont(new java.awt.Font("Comic Sans MS",1,15));
            regresar.setBounds(60, 500, 100, 30);
            panel.add(regresar);
            //guardar.addActionListener(this);
    }
 
 public void MostrarUsuario(){
       
     String linea;       
     FileReader leer;
     
    try{    
        archivo = new File("docUsuarios.txt");
        leer = new FileReader(archivo);           
        br = new BufferedReader(leer);  
        boolean verdad = false;
        while((linea = br.readLine()) != null){
               String persona [] = linea.split("\t");
               String Nusuarios = t1.getText();
                    if(persona[0].equals(Nusuarios)){
                    verdad = true;
                    bandera1 = true;
                    Usuario u = new Usuario(persona[0],persona[1],persona[2]);
                    cuadro2.append(linea+"\n");
                    br.read();
                    }
        }
        if(verdad == true){
                 JOptionPane.showMessageDialog(this,"Persona Encontrada");
        }
        if(verdad == false){
            JOptionPane.showMessageDialog(this,"Persona no Encontrada");
        }
          br.close();
          leer.close();
        }
        catch (Exception ex){
            JOptionPane.showMessageDialog(this,"No existe el archivo :C");
        }
    } 
    
 public void MostrarLibros(){
       
     String linea;        
     FileReader leer;
    try{    
        archivo = new File("docLibros.txt");
        leer = new FileReader(archivo);           
        br = new BufferedReader(leer);  
        boolean verdad = false;
        while((linea = br.readLine()) != null){
               String persona [] = linea.split("\t");
               String Nusuarios = t2.getText();
                    if(persona[0].equals(Nusuarios)){
                    verdad = true;
                    bandera2 = true;
                    Libros l = new Libros(persona[0],persona[1],persona[2]);
                    cuadro3.append(linea+"\n");
                    br.read();
                    }
        }
        if(verdad == true){
                 JOptionPane.showMessageDialog(this,"Libro Encontrado");
        }
        if(verdad == false){
            JOptionPane.showMessageDialog(this,"Libro no Encontrado");
        }
          br.close();
          leer.close();
        }
        catch (Exception ex){
            JOptionPane.showMessageDialog(this,"No existe el archivo :C");
        }
    } 
 
    public void MostrarPrestamo(){
       String linea;        
       FileReader leer;
       
         try{    

        archivo = new File("docPrestamos.txt");
        leer = new FileReader(archivo);           
        br = new BufferedReader(leer);
          while((linea = br.readLine()) !=null){
                              cuadro.append(linea+"\n");                
                }
          br.close();
          leer.close();
        }
        catch (Exception ex){
            
        }
    }
    
    public void Auxiliar(){
        if(bandera1 == true && bandera2 == true){
            guardar.setEnabled(true);
        }
    }
    
    public void Auxiliar2(){
        if(bandera1 == true){
            cuadro.setEnabled(true);
        }
    }
 
  public void Implementar(){

        Buscar1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                     MostrarUsuario();
                     Auxiliar();
                }
            });
        
               Buscar2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    MostrarLibros();
                    Auxiliar();
                }
            }); 
               
              guardar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
         
                String Nusuario = t1.getText();
                String TLibro = t2.getText();
                String Eusuario = t3.getText();
                String NELibro = t4.getText();
                String EDLibro = t5.getText();
                String diaE = dia.getSelectedItem().toString();
                String mesE = mes.getSelectedItem().toString();
                String añoE = año.getSelectedItem().toString();
                String diaD = diad.getSelectedItem().toString();
                String mesD = mesd.getSelectedItem().toString();
                String añoD = añod.getSelectedItem().toString();  
                
                Libros l = new Libros(TLibro, NELibro, EDLibro);
                Usuario u = new Usuario(Nusuario,Eusuario);
                libreria lib = new libreria(u,l, diaE, mesE, añoE, diaD, mesD, añoD);
                lib.GuardarListaPrestamos();
                MostrarPrestamo();
               Auxiliar2();
                }
            });
             
          regresar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                     MenuPrincipal mp = new MenuPrincipal();
                     mp.setVisible(true);
                     dispose();
                }
            });
  }
}
 /* @Override
    public void actionPerformed(ActionEvent ae) {
                String Nusuario = t1.getText();
                String TLibro = t2.getText();
                String Eusuario = t3.getText();
                String NELibro = t4.getText();
                String EDLibro = t5.getText();
                String diaE = (String) dia.getSelectedItem();
                String mesE = (String) mes.getSelectedItem();
                String añoE = (String) año.getSelectedItem();
                String diaD = (String) diad.getSelectedItem();
                String mesD = (String) mesd.getSelectedItem();
                String añoD = (String) añod.getSelectedItem();
                Libros l = new Libros(TLibro, NELibro, EDLibro);
                Usuario u = new Usuario(Nusuario,Eusuario);
                libreria lib = new libreria(u,l, diaE, mesE, añoE, diaD, mesD, añoD);
                lib.GuardarListaPrestamos();
    }*/
 
 