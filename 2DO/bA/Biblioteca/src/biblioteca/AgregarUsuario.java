package biblioteca;

import java.awt.Color;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class AgregarUsuario extends JFrame{
        JLabel t1, t2, t3;
        JPanel panel;
        JButton guardar, regresar;
        TextField nombre, edad, direccion;
        FileWriter escribir;
        Usuario user;
        
        public AgregarUsuario(){
                    this.setSize(390, 400);
                    this.setTitle("Biblioteca");
                    this.setResizable(false);
                    setLocationRelativeTo(null);
                    this.setLayout(null);
                    IniciarComponentes();
                    Implementar();
                    setDefaultCloseOperation(EXIT_ON_CLOSE);   
        }
        public void IniciarComponentes(){
            
                        panel = new JPanel();
                        panel.setLayout(null);
                        panel.setBackground(Color.blue);    
                        panel.setBounds(0, 0, 9999, 9999);
                        this.getContentPane().add(panel);
 
                           t1 = new JLabel("Nombre: ");
                           t1.setForeground(Color.black);
                           t1.setFont(new java.awt.Font("Cooper Black",1,15));
                           t1.setBounds(50, 50, 100, 20);
                           panel.add(t1);
                           
                           t2 = new JLabel("Edad: ");
                           t2.setForeground(Color.black);
                           t2.setFont(new java.awt.Font("Cooper Black",1,15));
                           t2.setBounds(50, 130, 100, 20);
                           panel.add(t2);
                           
                           t3 = new JLabel("Direccion: ");
                           t3.setForeground(Color.black);
                           t3.setFont(new java.awt.Font("Cooper Black",1,15));
                           t3.setBounds(50, 210, 100, 20);
                           panel.add(t3);
                           
                           this.add(nombre= new TextField());
                           nombre.setFont(new java.awt.Font("Comic Sans MS",1,15));
                           nombre.setBounds(50, 90, 280, 20);
                           nombre.setFocusable(true);
                           panel.add(nombre);
                           
                           this.add(edad= new TextField());
                           edad.setFont(new java.awt.Font("Comic Sans MS",1,15));
                           edad.setBounds(50, 170, 280, 20);
                           edad.setFocusable(true);
                           panel.add(edad);
                           
                           this.add(direccion= new TextField());
                           direccion.setFont(new java.awt.Font("Comic Sans MS",1,15));
                           direccion.setBounds(50, 250, 280, 20);
                           direccion.setFocusable(true);
                           panel.add(direccion);
                       
                            guardar = new JButton("Guardar.");
                            guardar.setFont(new java.awt.Font("Comic Sans MS",1,15));
                            guardar.setBounds(200, 300, 150, 30);
                            panel.add(guardar);
                            
                            regresar = new JButton("Regresar");
                            regresar.setFont(new java.awt.Font("Comic Sans MS",1,15));
                            regresar.setBounds(30, 300, 150, 30);
                            panel.add(regresar);
                            
                           // guardar.addActionListener(this);
        }
/*public void GuardarUsuario() {
            try{                       
                File archivo = new File("docUsuarios.txt");
                BufferedWriter bw;
                if(archivo.exists()){
                escribir = new FileWriter(archivo,true);
                bw = new BufferedWriter(escribir);    
                bw.newLine();
                bw.write(nombre.getText()+"\t"+edad.getText()+"\t"+direccion.getText());
                } 
                else{
                escribir = new FileWriter(archivo);
                bw = new BufferedWriter(escribir);
                bw.write(nombre.getText()+"\t"+edad.getText()+"\t"+direccion.getText());
                }                
                bw.close();
                escribir.close();
                    }
            catch (IOException ex){
                JOptionPane.showMessageDialog(this,"Archivo no encontrado");
            }
 }

public void IngresarUsuario(Usuario user){
    try{
        File f; 
        FileWriter fw;
        BufferedWriter buw;
        f= new File("docUsuarios.txt");
        if(f.exists()){
 
        fw = new FileWriter(f,true);
        buw = new BufferedWriter(fw);
        buw.newLine();
        buw.write(nombre.getText()+"\t"+edad.getText()+"\t"+direccion.getText());
        //buw.write(nombre.getText()+edad.getText()+direccion.getText());
        //buw.write(user.nombre+"\t"+user.edad+" \t"+user.direccion);
        }
        else{
        fw = new FileWriter(f);
        buw = new BufferedWriter(fw);
        buw.write(nombre.getText()+"\t"+edad.getText()+"\t"+direccion.getText()); 
        //buw.write(nombre.getText()+edad.getText()+direccion.getText());
        //buw.write(user.nombre+"\t"+user.edad+" \t"+user.direccion);
       }
        buw.close();
        fw.close();
    }
    catch(Exception ex){
          JOptionPane.showMessageDialog(this,"Archivo no encontrado");
    }
            
}*/
        
        public void Implementar(){
               regresar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                     MenuPrincipal mp = new MenuPrincipal();
                     mp.setVisible(true);
                     dispose();
                }
            });   
               
              guardar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                String Nusuario = nombre.getText();
                String Eusuario = edad.getText();
                String Dusuario = direccion.getText();
                Usuario u = new Usuario(Nusuario, Eusuario, Dusuario);
                u.GuardarUsuario();
                }
            }); 
        }
}
/*    @Override
    public void actionPerformed(ActionEvent ae) {
                String Nusuario = nombre.getText();
                String Eusuario = edad.getText();
                String Dusuario = direccion.getText();
                Usuario u = new Usuario(Nusuario, Eusuario, Dusuario);
                u.GuardarUsuario();
    }
}*/
