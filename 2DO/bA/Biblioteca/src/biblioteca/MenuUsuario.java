package biblioteca;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class MenuUsuario extends JFrame{
    
          JPanel panel;
          JButton boton1, boton2, boton3, boton4;
          JLabel texto;
        public MenuUsuario(){
                    this.setSize(400, 400);
                    this.setTitle("La Biblioteca");
                    this.setResizable(false);
                    setLocationRelativeTo(null);
                    this.setLayout(null);
                    IniciarComponentes();
                    Implementar();
                    setDefaultCloseOperation(EXIT_ON_CLOSE);   
        }
        
        public void IniciarComponentes(){
                panel = new JPanel();
                panel.setLayout(null);
                panel.setBackground(Color.BLACK);    
                panel.setBounds(0, 0, 9999, 9999);
                this.getContentPane().add(panel);

                boton1 = new JButton("Usuarios");
                boton2 = new JButton("Libros");
                boton3 = new JButton("Préstamos");
                boton4 = new JButton("Devoluciones");
                
                boton1.setFont(new java.awt.Font("Comic Sans MS",1,15));
                boton1.setBounds(30, 160, 150, 30);
                panel.add(boton1);
                
                boton2.setFont(new java.awt.Font("Comic Sans MS",1,15));
                boton2.setBounds(210, 160, 150, 30);
                panel.add(boton2);
                
                boton3.setFont(new java.awt.Font("Comic Sans MS",1,15));
                boton3.setBounds(30, 220, 150, 30);
                panel.add(boton3);
                
                boton4.setFont(new java.awt.Font("Comic Sans MS",1,15));
                boton4.setBounds(210, 220, 150, 30);
                panel.add(boton4);
                
                texto = new JLabel("Menú Principal");
                texto.setForeground(Color.YELLOW);
                texto.setFont(new java.awt.Font("Cooper Black",1,30));
                texto.setBounds(40, 100, 350, 30);
                panel.add(texto);
            }
        public void Implementar(){
            boton1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                        MenuUsuarios mu = new MenuUsuarios();
                        mu.setVisible(true);
                }
            });
            boton2.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent ae) {
                }
                
            });
           boton3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
               
                }
            });
            boton4.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent ae) {
                }
                
            });
            this.dispose();
        }
}
