package biblioteca;

import java.awt.Color;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class AgregarLibro extends JFrame{
     JLabel t1, t2, t3, t4;
        JPanel panel;
        JButton guardar, regresar;
        TextField titulo, editorial, numejemplar;
        FileWriter escribir;
        
        public AgregarLibro(){
                    this.setSize(390, 400);
                    this.setTitle("Biblioteca");
                    this.setResizable(false);
                    setLocationRelativeTo(null);
                    this.setLayout(null);
                    IniciarComponentes();
                    Implementar();
                    setDefaultCloseOperation(EXIT_ON_CLOSE);   
        }
        public void IniciarComponentes(){
                            
                        panel = new JPanel();
                        panel.setLayout(null);
                        panel.setBackground(Color.white);    
                        panel.setBounds(0, 0, 9999, 9999);
                        this.getContentPane().add(panel);
 
                           t1 = new JLabel("Titulo: ");
                           t1.setForeground(Color.red);
                           t1.setFont(new java.awt.Font("Cooper Black",1,15));
                           t1.setBounds(50, 50, 100, 20);
                           panel.add(t1);
                           
                           t2 = new JLabel("Editorial: ");
                           t2.setForeground(Color.red);
                           t2.setFont(new java.awt.Font("Cooper Black",1,15));
                           t2.setBounds(50, 130, 100, 20);
                           panel.add(t2);
                           
                           t3 = new JLabel("Numero de Ejemplar: ");
                           t3.setForeground(Color.red);
                           t3.setFont(new java.awt.Font("Cooper Black",1,15));
                           t3.setBounds(50, 210, 220, 20);
                           panel.add(t3);
                           
                           this.add(titulo= new TextField());
                           titulo.setFont(new java.awt.Font("Comic Sans MS",1,15));
                           titulo.setBounds(50, 90, 280, 20);
                           titulo.setFocusable(true);
                           panel.add(titulo);
                           
                           this.add(editorial= new TextField());
                           editorial.setFont(new java.awt.Font("Comic Sans MS",1,15));
                           editorial.setBounds(50, 170, 280, 20);
                           editorial.setFocusable(true);
                           panel.add(editorial);
                           
                           this.add(numejemplar= new TextField());
                           numejemplar.setFont(new java.awt.Font("Comic Sans MS",1,15));
                           numejemplar.setBounds(50, 250, 280, 20);
                           numejemplar.setFocusable(true);
                           panel.add(numejemplar);
                       
                            guardar = new JButton("Guardar.");
                            guardar.setFont(new java.awt.Font("Comic Sans MS",1,15));
                            guardar.setBounds(200, 300, 150, 30);
                            panel.add(guardar);
                           
                            regresar = new JButton("Regresar");
                            regresar.setFont(new java.awt.Font("Comic Sans MS",1,15));
                            regresar.setBounds(30, 300, 150, 30);
                            panel.add(regresar);
                            
                            
//                            guardar.addActionListener(this);
        }
/*protected void GuardarLibro() {
            try{                       
                File archivo = new File("docLibros.txt");
                BufferedWriter bw;
                if(archivo.exists()){
                escribir = new FileWriter(archivo,true);
                bw = new BufferedWriter(escribir);    
                bw.newLine();
                bw.write(titulo.getText()+"\t"+editorial.getText()+"\t"+numejemplar.getText());

                } else{
                escribir = new FileWriter(archivo.getPath());
                bw = new BufferedWriter(escribir);                  
                bw.write(titulo.getText()+"\t"+editorial.getText()+"\t"+numejemplar.getText());                 
                }                
                bw.close();
                escribir.close();

                    }
            catch (IOException ex){
                
            }
 } */  
        
    public void Implementar(){
                regresar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                     MenuPrincipal mp = new MenuPrincipal();
                     mp.setVisible(true);
                     dispose();
                }
            });       
             guardar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                String Tlibro = titulo.getText();
                String Elibro = editorial.getText();
                String Nlibro = numejemplar.getText();
                Libros l = new Libros(Tlibro, Elibro, Nlibro);
                l.GuardarLibro();
                }
                
            });
    }
    /*@Override
    public void actionPerformed(ActionEvent ae) {
                String Tlibro = titulo.getText();
                String Elibro = editorial.getText();
                String Nlibro = numejemplar.getText();
                Libros l = new Libros(Tlibro, Elibro, Nlibro);
                l.GuardarLibro();
    }*/
        
    
    
}
