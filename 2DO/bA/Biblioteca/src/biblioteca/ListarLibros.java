package biblioteca;

import java.awt.Color;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ListarLibros extends JFrame{

    JPanel panel;
    TextArea cuadro;
    File archivo;
    FileReader leer;
    BufferedReader br;
    JButton mostrar,regresar;    
    
    public ListarLibros(){
                     this.setSize(500, 400);
                    this.setTitle("Biblioteca");
                    this.setResizable(false);
                    setLocationRelativeTo(null);
                    this.setLayout(null);
                    IniciarComponentes();
                    Implementar();
                    setDefaultCloseOperation(EXIT_ON_CLOSE); 
    }
    
    public void IniciarComponentes(){
        panel = new JPanel();
                        panel.setLayout(null);
                        panel.setBackground(Color.black);    
                        panel.setBounds(0, 0, 9999, 9999);
                        this.getContentPane().add(panel);
                        
                        cuadro = new TextArea();
                        cuadro.setBackground(Color.white);
                        cuadro.setBounds(5, 5 , 485, 250);
                        cuadro.setForeground(Color.black);
                        panel.add(cuadro);
                        
                        mostrar = new JButton("Mostrar.");
                        mostrar.setFont(new java.awt.Font("Comic Sans MS",1,15));
                        mostrar.setBounds(300, 300, 150, 30);
                        panel.add(mostrar);  
                        
                        regresar = new JButton("Regresar");
                        regresar.setFont(new java.awt.Font("Comic Sans MS",1,15));
                        regresar.setBounds(50, 300, 150, 30);
                        panel.add(regresar);
    }
    
    public boolean MostrarLibro(){
        
  
         String linea;        
   
         try{    

        archivo = new File("docLibros.txt");
        leer = new FileReader(archivo);           
        br = new BufferedReader(leer);
          while((linea = br.readLine()) !=null){
                              cuadro.append(linea+"\n");                
                }
          br.close();
          leer.close();
          return true;
        }
        catch (Exception ex){
            
        }
        return false;
    }
  
  public void Implementar(){
        regresar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                     MenuPrincipal mp = new MenuPrincipal();
                     mp.setVisible(true);
                     dispose();
                }
            });
        
          mostrar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                        MostrarLibro();
                }
            });
  }

}