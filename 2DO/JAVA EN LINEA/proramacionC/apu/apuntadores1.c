#include <stdio.h> 
int main() { 
    int a=5; 
    //Declaración de variable entera de tipo entero a
    int *puntero; //Declaración de variable puntero de tipo entero 
    puntero = &a; //Asignación de la dirección memoria de a 
    printf("El valor de a es: %d. ",a);
    printf("\nEl valor de *puntero es: %d. \n",a,*puntero); 
    printf("La direccion de memoria de *puntero es: %p",puntero); 
    return 0; 
    }
/*#include <stdio.h> 
int main() { 
    int a=5; 
    //Declaración de variable entera de tipo entero 
    int *puntero; //Declaración de variable puntero de tipo entero 
    puntero = &a; //Asignación de la dirección memoria de a 
    printf("El valor de a es: %d. \nEl valor de *puntero es: %d. \n",a,*puntero); 
    printf("La direccion de memoria de *puntero es: %p",puntero); 
    return 0; 
    }*/