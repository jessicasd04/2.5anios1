#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */
float tri(float b, float h){
	 float A;
		A=(b*h)/2;
		return A;
}
float cir(float r){
	float A;
		A=3.1416*r*r;
		return A;
}
float pen(float a, float l){
	float A,P;
			P=5*l;
			A=(P*a)/2;
			return A;
}
float oct(float a, float l){
	float A,P;
			P=8*l;
			A=(P*a)/2;
			return A;
}
int main(int argc, char *argv[]) {
		int x;
		float A,b,h,r,l,a; 
	printf("\n\tBIENVENIDO AL CALCULO DE AREAS\t\n \n�QUE DESEA HACER?\n");
	printf("\n1.-AREA DE UN TRIANGULO \n2.-AREA DE UN CIRCULO\n3.-AREA DE UN PENTAGONO\n4.-AREA DE UN OCTAGONO\n \nOPRIMA EL NUMERO QUE DESEA  \n");
	scanf("%d",&x);
	switch (x){
		case 1: 
			printf("\n \tAREA DEL TRIANGULO\t \n");
			printf("\nDigite la base y la altura\n");
			scanf("%f %f",&b,&h);
			A= tri(b,h);
		    printf("\n El area del triangulo es %f",A);	
			break;
		case 2:
			printf("\n \tAREA DE UN CIRCULO\t \n");
			printf("\n Digite el radio\n");
			scanf("%f",&r);
			A=cir(r);
			printf("El area del circulo es %f",A);		
			break;
		case 3:	
			printf("\n \tAREA DE UN PENTAGONO\t \n");
			printf("\nDigite el apotema y el lado\n");
			scanf("%f %f",&a,&l);
			A=pen(a,l);			
			printf("\n El area del pentagono es %f",A);
			break;
		case 4:	
			printf("\n \tAREA DE UN OCTAGONO\t \n");
			printf("\nDigite el apotema y el lado\n");
			scanf("%f %f",&a,&l);
			A=oct(a,l);
			printf("\n El area del octagono es %f",A);
			break;
		}
		
	return 0;
}
