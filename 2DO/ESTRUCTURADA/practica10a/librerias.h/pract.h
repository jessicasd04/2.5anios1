

#include<stdio.h>
#include<stdlib.h>


#define celcuis_a_Fahrenheit 30.5F
#define faren_a_celcuis 85.2F
#define celcuisaKelvin 75.3F
#define kelvinafaren 79.1F
#define kelvinacelcuis 46.12F
#define farenaKelvin 52.5F;
#define metros_a_Pies 5.1F
#define pies_a_Metros 88.12F
#define Metros_a_Yardas 4.3F
#define yardas_a_Metros 3.6F
#define PiesaYardas 2.74F
#define Yardas_a_Pies 5.325F
#define Kilogramos_a_Libras 12.47F
#define LibrasaKilogramos 5.1F
#define Kilogramos_aOnzas 14.12F
#define LibrasaOnzas 1.8F
#define OnzasaLibras 20.9F
#define Onzasakilogramos 100.95F
#define GalonesaLitros 9.18F

void conversionesGrados(float celcius,float faren, float kelvin);
void conversionesMedidas(float metro, float pies, float yardas, float kilo, float libra, float onzas,float litros, float galones);
void conversionesMonedas(float pesos, float dolares, float euros);

