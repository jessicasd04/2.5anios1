#include<stdio.h>
#include<stdlib.h>


#define celcuis_a_Fahrenheit 30.5F
#define faren_a_celcuis 85.2F
#define celcuisaKelvin 75.3F
#define kelvinafaren 79.1F
#define kelvinacelcuis 46.12F
#define farenaKelvin 52.5F;
#define metros_a_Pies 5.1F
#define pies_a_Metros 88.12F
#define Metros_a_Yardas 4.3F
#define yardas_a_Metros 3.6F
#define PiesaYardas 2.74F
#define Yardas_a_Pies 5.325F
#define Kilogramos_a_Libras 12.47F
#define LibrasaKilogramos 5.1F
#define Kilogramos_aOnzas 14.12F
#define LibrasaOnzas 1.8F
#define OnzasaLibras 20.9F
#define Onzasakilogramos 100.95F
#define GalonesaLitros 9.18F

 void conversionesGrados(float celcius,float faren, float kelvin){
int opc;
 printf("\t\t\tCONVERSION DE GRADOS\n\n");
 printf("1.- Grados Centigrados a Fahrenheit\n\n");
 printf("2.- Grados Centigrados a Kelvin\n\n");
 printf("3.- Grados Fahrenheit a Centigrados\n\n");
 printf("4.- Grados Fahrenheit a Kelvin\n\n");
 printf("5.- Grados Kelvin a Centigrados\n\n");
 printf("6.- Grados Kelvin a Fahrenheit\n\n");
 printf("7.- Salir\n");
 printf("Seleccione la opcion deseada: \n\n");
 scanf(" %d", &opc);
 system("cls");

 switch(opc){
 case 1:
 printf("\t\tGrados Centigrados a Fahrenheit\n\n");
 printf("Ingrese los grados Centigrados: ");
 scanf(" %f", &celcius);
 faren= (celcius*(9/5)+32);
 printf("\n\n");
 printf("%.2f en grados Fahrenheint %.2f", celcius, faren);
 printf("\n\n");

 break;
 case 2:
 printf("\t\tGrados Centigrados a Kelvin\n\n");
 printf("Ingrese los grados Centigrados: ");
 scanf(" %f", &celcius);
 kelvin= (celcius+273.15);
 printf("\n\n");
 printf("%.2f en grados Kelvin %.2f", celcius, kelvin);
 printf("\n\n");

 break;

 case 3:
 printf("\t\tGrados Fahrenheit a Centigrados\n\n");
 printf("Ingrese los grados Fahrenheit: ");
 scanf(" %f", &faren);
 celcius=((faren-32)*0.5555555556);
 printf("%.2f en grados Centigrados %.2f ", faren, celcius);
 printf("\n\n");

 break;

 case 4:
 printf("\t\tGrados Fahrenheit a Kelvin\n\n");
 printf("Ingrese los grados Fahrenheit: ");
 scanf(" %f", &faren);
 kelvin=((faren+459.67)*0.5555555556);
 printf("\n\n");
 printf("%.2f en grados Kelvin %.2f", faren, kelvin);
 printf("\n\n");
 break;
 case 5:
 printf("\t\tGrados Kelvin a Centigrados\n\n");
 printf("Ingrese los grados Kelvin: ");
 scanf(" %f", &kelvin);
 celcius=(kelvin-273.15);
 printf("\n\n");
 printf("%.2f en grados Kelvin %.2f", kelvin, celcius);
 printf("\n\n");

 break;
 case 6:
 printf("\t\tGrados Kelvin a Fahrenheit\n\n");
 printf("Ingrese los grados Kelvin: ");
 scanf(" %f", &kelvin);
 faren=((kelvin-273.15)*33.8);
 printf("\n\n");
 printf("%.2f en grados Kelvin %.2f", kelvin, faren);
 printf("\n\n");

 break;

 case 7:
 printf("\n SALIR\n");
 break;

 default:
break;

 }
 }
void conversionesMedidas(float metro, float pies, float yardas, float kilo, float libra, float onzas,
float litros, float galones){
int opcion;
 printf("\t\t\tCONVERSION DE MEDIDAS\n\n");
 printf("1.- Metros a Pies \n\n");
 printf("2.- Metros a Yardas\n\n");
 printf("3.- Pies a Metros\n\n");
 printf("4.- Yardas a Metros\n\n");
 printf("5.- Pies a Yardas\n\n");
 printf("6.- Yardas a Pies\n\n");
 printf("7.- Kilogramos a Libras\n\n");
 printf("8.- Libras a Kilogramos\n\n");
 printf("9.- Kilogramos a Onzas\n\n");
 printf("10.- Libras a Onzas\n\n");
 printf("11.- Onzas a Libras\n\n");
 printf("12.- Onzas a Kilogramos\n\n");
 printf("13.- Litros a Galones\n\n");
 printf("14.- Galones a Litros\n\n");
 printf("15.- Salir \n\n");
 printf("Seleccione la opcion deseada: \n\n");
 scanf(" %d", &opcion);
 system("cls");
 switch(opcion){
 case 1:
 printf("\t\tMetros a Pies\n\n");
 printf("Ingrese los Metros: ");
 scanf(" %f", &metro);
 pies= (metro*3.28);
 printf("\n\n");
 printf("%.2f la medida en Pies es %.2f", metro, pies);
 printf("\n\n");
break;
 case 2:

 printf("\t\tMetros a Yardas\n\n");
 printf("Ingrese los Metros: ");
 scanf(" %f", &metro);
 yardas= (metro*1.093);
 printf("\n\n");
 printf("%.2f la medida en Yardas es %.2f", metro, yardas);
 printf("\n\n");
 break;

 case 3:

 printf("\t\tPies a Metros\n\n");
 printf("Ingrese los Pies: ");
 scanf(" %f", &pies);
 metro=(pies*0.3048);
 printf("\n\n");
 printf("%.2f la medida en Metros es %.2f", pies, metro);
 printf("\n\n");
 break;
 case 4:

 printf("\t\tYardas a Metros\n\n");
 printf("Ingrese las Yardas: ");
 scanf(" %f", &yardas);
 metro=(yardas*0.9144);
 printf("\n\n");
 printf("%.2f la medida en Metros es %.2f", yardas, metro);
 printf("\n\n");
 break;
 case 5:

 printf("\t\tPies a Yardas\n\n");
 printf("Ingrese los Pies: ");
 scanf(" %f", &pies);
 yardas=(pies*0.3333);
 printf("\n\n");
 printf("%.2f la medida en Yardas es %.2f", pies, yardas);
 printf("\n\n");
 break;
 case 6:

 printf("\t\tYardas a Pies\n\n");
 printf("Ingrese las Yardas: ");
 scanf(" %f", &yardas);
 pies=(yardas*3);
 printf("\n\n");
 printf("%.2f la medida en Pies es %.2f", yardas, pies);
 printf("\n\n");
 break;
 case 7:

 printf("\t\tKilogramos a Libras\n\n");
 printf("Ingrese los Kilogramos: ");
 scanf(" %f", &kilo);
 libra=(kilo*2.204);
 printf("\n\n");
 printf("%.2f la medida en Libras es %.2f", kilo, libra);
 printf("\n\n");
 break;
case 8:
 printf("\t\tLibras a Kilogramos\n\n");
 printf("Ingrese las Libras: ");
 scanf(" %f", &libra);
 kilo=(libra*0.4535);
 printf("\n\n");
 printf("%.2f la medida en Kilogramos es %.2f", libra, kilo);
 printf("\n\n");
 break;

case 9:
 printf("\t\tKilogramos a Onzas\n\n");
 printf("Ingrese los Kilogramos: ");
 scanf(" %f", &kilo);
 onzas=(kilo*35.274);
 printf("\n\n");
 printf("%.2f la medida en Onzas es %.2f", kilo, onzas);
 printf("\n\n");
 break;
 case 10:

 printf("\t\tLibras a Onzas\n\n");
 printf("Ingrese las Libras: ");
 scanf(" %f", &libra);
 onzas=(libra*16);
 printf("\n\n");
 printf("%.2f la medida en Onzas es %.2f", libra, onzas);
 printf("\n\n");
 break;
case 11:
 printf("\t\tOnzas a Libras\n\n");
 printf("Ingrese las Onzas: ");
 scanf(" %f", &onzas);
 libra=(onzas*0.0625);
 printf("\n\n");
 printf("%.2f la medida en Libra es %.2f", onzas, libra);
 printf("\n\n");
 break;
 case 12:

 printf("\t\tOnzas a Kilogramos\n\n");
 printf("Ingrese las Onzas: ");
 scanf(" %f", &onzas);
 kilo=(onzas*0.02834);
 printf("\n\n");
 printf("%.2f la medida en Kilogramos es %.2f", onzas, kilo);
 printf("\n\n");
 printf("\n\n");
 break;
 case 15:
 printf("\nSALIR\n");
 break;
 default:
 break;
 }



}
void conversionesMonedas(float pesos, float dolares, float euros){
int opcion;

 printf("\t\t\tCONVERSION DE MONEDAS\n\n");
 printf("1.- Pesos a Dolares\n\n");
 printf("2.- Pesos a Euros\n\n");
 printf("3.- Dolares a Pesos\n\n");
 printf("4.- Euros a Pesos\n\n");
 printf("5.- Dolares a Euros\n\n");
 printf("6.- Euros a Dolares\n\n");
 printf("7.- Salir\n\n");
 printf("Seleccione la opcion deseada: \n\n");
 scanf(" %d", &opcion);
 system("cls");

 switch(opcion){
 case 1:
 printf("\t\tPesos a Dolares\n\n");
 printf("Ingrese los Pesos: ");
 scanf(" %f", &pesos);
 dolares= (pesos*0.05414);
 printf("\n\n");
 printf("%.2f el cambio en Dolares es %.2f", pesos, dolares);
 printf("\n\n");
 break;
 case 2:
 printf("\t\tPesos a Euros\n\n");
 printf("Ingrese los Pesos: ");
 scanf(" %f", &pesos);
 euros= (pesos*0.04388);
 printf("\n\n");
 printf("%.2f el cambio en Dolares es %.2f", pesos, euros);
 printf("\n\n");

 break;

 case 3:
 printf("\t\tDolares a Pesos\n\n");
 printf("Ingrese los Dolares: ");
 scanf(" %f", &dolares);
 pesos=(dolares*18.5);
 printf("\n\n");
 printf("%.2f el cambio en pesos es %.2f", dolares, pesos);
 printf("\n\n");

 break;

 case 4:
 printf("\t\tEuros a Pesos\n\n");
 printf("Ingrese los Euros: ");
 scanf(" %f", &euros);
 pesos=(euros*22.763);
 printf("\n\n");
 printf("%.2f el cambio en Pesos es %.2f", euros, pesos);
 printf("\n\n");

 break;
 case 5:
 printf("\t\tDolares a Euros\n\n");
 printf("Ingrese los Dolares: ");
 scanf(" %f", &dolares);
 euros=(dolares*0.81055);
 printf("\n\n");
 printf("%.2f el cambio en Euros es %.2f", dolares, euros);
 printf("\n\n");

 break;
 case 6:
 printf("\t\tEuros a Dolares\n\n");
 printf("Ingrese los Euros: ");
 scanf(" %f", &euros);
 dolares=(euros*1.2337);
 printf("\n\n");
 printf("%.2f el cambio en Dolares es %.2f", euros, dolares);
 printf("\n\n");

 break;

 case 7:

 default:
     printf("\nsalir\n");
  break;
 }
}

